<?php include 'header.php'; ?>

<div id="titleRow">
  <div class="container">
    <h1>
      <span class="lft"></span>
      <span class="mid">TIGER Grant Application Materials</span>
      <span class="rt"></span>
    </h1>
  </div>
</div>

<div id="tiger-grant-materials">
    <p><strong>1.</strong> <a href="docs/narrative.pdf" target="_blank">Outer Powell Transportation Safety and Livability TIGER 2016 Grant Narrative</a> (PDF, 2.7MB)</p>
    <p><strong>2.</strong> <a href="docs/tigerinfo.pdf" target="_blank">TIGER Info Summary</a> (PDF, 236KB)</p>
    <p><strong>3.</strong> <a href="docs/CrossSec.pdf" target="_blank">Proposed Cross Sections</a> (PDF, 198KB)</p>
    <p><strong>4.</strong> <a href="docs/letters.pdf" target="_blank">Letters of Support</a> (PDF, 9.7MB)</p>
    <p><strong>5.</strong> <a href="docs/BCAmethod.pdf" target="_blank">BCA Methodology</a> (PDF, 442KB)</p>
    <p><strong>6.</strong> <a href="docs/BCASpread.xls" target="_blank">BCA Analysis Spreadsheets</a> (XLS, 16.2MB)</p>
    <p><strong>7.</strong> <a href="docs/PIsummary.pdf" target="_blank">Public Involvement Summary</a> (PDF, 4.2MB)</p>
    <p><strong>8.</strong> <a href="docs/API.pdf" target="_blank">Project Area of Potential Impact (API)</a> (PDF, 6.9MB)</p>
    <p><strong>9.</strong> <a href="docs/OPCDP.pdf" target="_blank">Outer Powell Conceptual Design Plan</a>  (PDF, 5.4MB)</p>
    <p><strong>10.</strong> <a href="docs/MOUs.pdf" target="_blank">Interagency Agreements and MOUs</a> (PDF, 362KB)</p>
    <p><strong>11.</strong> <a href="docs/risk.pdf" target="_blank">Risk Management Matrix</a> (PDF, 74KB)</p>
    <p><strong>12.</strong> <a href="docs/wagecert.pdf" target="_blank">Federal Wage Rate Certification</a> (PDF, 238KB)</p>
</div>

<style type="text/css">
div#tiger-grant-materials { font-size: 1.2em; margin: 0 auto; width: 40%; }
div#tiger-grant-materials strong { font-size: 1.3em; margin-left: -26px; margin-right: 8px; position: relative; }
div#tiger-grant-materials p { margin-left: 16px; margin-bottom: 14px; }
</style>

<?php include 'footer.php'; ?>
