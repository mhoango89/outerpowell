<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Outer Powell Transportation Safety Project</title>
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico" />
	<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="../bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.17.0/TweenMax.min.js"></script>


	<!-- fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,900italic,400italic' rel='stylesheet' type='text/css'>

<?php
  $current_page = basename($_SERVER['PHP_SELF']);
?>

<meta name="google-translate-customization" content="280bb36c403b144a-9c2bd54ffd4bf67a-g476bcfafedcc5af2-19"></meta>
</head>
<body>

<!-- Start Main Menu and Logo-->
	<div class="navbar navbar-static-top">
		<div class="navbar-inner">
			<div class="container">
                <!-- Start Logo -->
				<a href="../index.php" class="brand active"><img src="../img/logo.png" alt="Quartum!">Outer Powell</a>
                <!-- End Logo -->

				<a class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse">menu <span class="caret"></span></a>
				<div class="nav-collapse">
				    <ul class="nav pull-right">
                        <li><a href="../index.php">Home</a></li>
                        <li class="dropdown"><a href="../about.php" class="dropdown-toggle">About</a>
						<ul class="dropdown-menu unstyled">
						        <li><a href="../schedule.php">Project Schedule</a></li>
					        </ul>
						</li>
					    <li class="dropdown"><a href="../get-involved.php" class="dropdown-toggle">Get Involved</a>
						<ul class="dropdown-menu unstyled">
						        <li><a href="../get-involved.php">Attend a meeting</a></li>
						        <li><a href="../contact.php">Submit a comment</a></li>
					        </ul>
						</li>
						<li class="active"><a href="../resources.php">Resources</a></li>
						<!-- <li class="<?php if ($current_page == "news.php"){ echo "active "; }?>"><a href="news.php">News</a></li> -->
					    <li><a href="../contact.php">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- <div id="map-comment-button"><a href="http://www.outerpowellsafety.org/map.php">Submit a Map Comment</a></div> -->
