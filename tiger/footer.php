<div class="workTogether">
	<div class="container">
		<div class="box">
			<div class="row">
				<div class="span7">
					<p>Tell us what you think! We value your input.</p>
				</div>
				<div class="span5">
					<a href="../contact.php" class="btn red-btn">Submit a comment</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="contactUsRow" class="green-row">
	<div class="container">
		<div class="row">
			<div class="span3" style="margin-right:5%;">

				<ul class="thumbnails">
					<li>
						<a href="http://www.oregon.gov/ODOT/HWY/REGION1/Pages/ProjectPage.aspx" target="_blank">
							<img src="../img/odot-horizontal.png" class="shrinkimageie" alt="">
						</a>
						<br>
						<br>
						<p><a href="http://www.oregon.gov/ODOT/HWY/REGION1/Pages/ProjectPage.aspx" target="_blank">ODOT Region 1 Projects</a></p>
					</li>
				</ul>

			</div>

			<div class="span3">
				<h4>Contact us</h4>
				<address>
					<strong>Address:</strong> Susan Hanson, ODOT Community Affairs
					<br> 123 NW Flanders Street
					<br>Portland, OR 97209
					<br>
					<strong>Tel:</strong> 503-731-3490
					<br>
					<strong>Email:</strong> <a href="#">info@outerpowellsafety.org</a>
				</address>

			</div>


			<div class="span3">
				<div id="getInTouch">
					<h4>Follow Us</h4>
					<p><a href="https://www.facebook.com/OregonDOT" target="_blank">ODOT Facebook</a></p>



					<ul class="social">
						<!-- <li><a href="#" class="facebook" data-toggle="tooltip" title="Facebook"></a></li>
              <li><a href="#" class="twitter" data-toggle="tooltip" title="Twitter"></a></li>
			  <li><a href="#" class="youtube" data-toggle="tooltip" title="You Tube"></a></li>
			  <li><a href="#" class="rss" data-toggle="tooltip" title="RSS"></a></li>
			  <li><a href="#" class="instagram" data-toggle="tooltip" title="Instagram"></a></li>

			  <li><a href="#" class="linkedin" data-toggle="tooltip" title="LinkedIn"></a></li>
			   <li><a href="#" class="googleplus" data-toggle="tooltip" title="Google+"></a></li>
              <li><a href="#" class="pinterest" data-toggle="tooltip" title="Pinterest"></a></li>
              <li><a href="#" class="delicious" data-toggle="tooltip" title="Delicious"></a></li>
              <li><a href="#" class="deviantart" data-toggle="tooltip" title="Deviant Art"></a></li>
              <li><a href="#" class="digg" data-toggle="tooltip" title="Digg"></a></li>
              <li><a href="#" class="flickr" data-toggle="tooltip" title="Flickr"></a></li>
              <li><a href="#" class="lastfm" data-toggle="tooltip" title="LastFm"></a></li>
              <li><a href="#" class="rss" data-toggle="tooltip" title="RSS"></a></li>
              <li><a href="#" class="skype" data-toggle="tooltip" title="Skype"></a></li>
              <li><a href="#" class="stumbleupon" data-toggle="tooltip" title="Stumbleupon"></a></li>
              <li><a href="#" class="tumblr" data-toggle="tooltip" title="Tumblr"></a></li>
              <li><a href="#" class="youtube" data-toggle="tooltip" title="You Tube"></a></li>
              <li><a href="#" class="vimeo" data-toggle="tooltip" title="Vimeo"></a></li>
              <li><a href="#" class="dribbble" data-toggle="tooltip" title="Dribbble"></a></li>
              <li><a href="#" class="behance" data-toggle="tooltip" title="Behance"></a></li>
              <li><a href="#" class="dailymotion" data-toggle="tooltip" title="Dailymotion"></a></li>
              <li><a href="#" class="instagram" data-toggle="tooltip" title="Instagram"></a></li>
              -->
					</ul>


				</div>
			</div>

			<div class="span2">
				<div id="newsletter">
					<h4>Translate Us</h4>
					<div id="google_translate_element"></div>
					<script type="text/javascript">
						function googleTranslateElementInit() {
							new google.translate.TranslateElement({
								pageLanguage: 'en',
								layout: google.translate.TranslateElement.InlineLayout.SIMPLE
							}, 'google_translate_element');
						}
					</script>
					<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

				</div>
			</div>




		</div>
	</div>
</div>
<!-- Start Footer -->
<div id="footText">
	<div class="container">
		<div class="row">
			<div class="span4">
				<a href="#" class="toTop pull-left">BACK TO TOP</a>
			</div>
			<div class="span8">
				<p class="pull-right">&copy; 2014 Oregon Department of Transportation.</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- End Footer -->


<script src="../js/jquery.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../js/jquery.contenthover.min.js"></script>
<script src="../js/jquery.tweet.js"></script>
<script src="../js/jquery.colorbox-min.js"></script>
<script src="../js/gsapanimations.js"></script>

<script type="text/javascript">
	$(window).load(function() {
		/***  jQuery ContentHover Plugin ***/
		$('ul.withHover img').contenthover({
			overlay_opacity: 0.5
		});


		/***  Footer Back to Top link ***/
		$('a.toTop').click(function() {
			$('html, body').animate({
				scrollTop: '0px'
			}, 300);
			return false;
		});

		/*** Carousel Bootstrap slider ***/
		$('#quartumCarousel').carousel({
			interval: 7000,
			pause: "hover"
		})

	});


	jQuery(document).ready(function() {

		/* tooltip init */
		jQuery("[data-toggle='tooltip']").tooltip();

		/*colorbox*/
		jQuery('a.colorUp').colorbox({
			maxWidth: "95%"
		});

		/* twitter */
		jQuery('.tweets').tweet({
			template: "{text} {time}",
			//li_class: " ",
			twitter_api_url: 'twitter/proxy/twitter_proxy.php'
		});
	});


	$(window).load(function(){
		$("#google_translate_element").css({
			"display": "inline-block"
		});
		$("#google_translate_element").change(function(){
			$("#google_translate_element").css({
				"display": "inline-block"
			});
		});
	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57335515-1', 'auto');
  ga('send', 'pageview');
</script>
</body>
</html>
