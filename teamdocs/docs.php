<?php include 'header.php'; ?>

<div id="titleRow">
    <div class="container nobottommargin">
        <h1 class="work">
            <span class="lft"></span><span class="mid">Team Documents</span><span class="rt"></span>
        </h1>
    </div>
</div>

<div class="singleProject">

    <div class="container">

       <div class="tabs1">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#recent" data-toggle="tab">Agency Working Group</a></li>
                <li><a href="#factsheets" data-toggle="tab">Community Advisory Group</a></li>
				<li><a href="#resources" data-toggle="tab">General Resources</a></li>
            </ul>

            <div class="tab-content">
	
                <div class="tab-pane active" id="recent">
                    <ul class="documents">
					<li><a href="doc/DesignConstraints&Concepts.pdf">Design Constraints Report</a></li>
					<li><a href="doc/OPSTP_ AWG_Mtg_Packet_12.3.14.pdf">AWG Meeting Packet #1</a></li>
					<li><a href="doc/OPTSP_AWG_Mtg_2_Packet_2015_02_04.pdf">AWG Meeting Packet #2</a></li>
					</ul>
                </div>
				
               <div class="tab-pane" id="factsheets">
                    <ul class="documents">
				
					
					</ul>
                </div>
				
				<div class="tab-pane" id="resources">
                    <ul class="documents">
					<li><a href="../doc/infographic.pdf">Infographic</a></li>
					<li><a href="../doc/conceptual-design-plan.pdf">Conceptual Design Plan</a></li>
					<li><a href="../doc/factsheet-english.pdf">Factsheet (English)</a></li>
					<li><a href="../doc/factsheet-russian.pdf">Factsheet (Russian)</a></li>
					<li><a href="../doc/factsheet-vietnamese.pdf">Factsheet (Vietnamese)</a></li>
					<li><a href="../doc/factsheet-spanish.pdf">Factsheet (Spanish)</a></li>
					<li><a href="../doc/factsheet-chinese.pdf">Factsheet (Chinese)</a></li>
		
					</ul>

                </div>
            </div>
        </div>
		 </div>
		
		
		
    </div>



<?php include 'footernocomment.php'; ?>