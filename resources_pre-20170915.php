<?php include 'header.php'; ?>

<div id="titleRow">
  <div class="container">
    <h1>
      <span class="lft"></span>
      <span class="mid">Resources</span>
      <span class="rt"></span>
    </h1>
  </div>
</div>

<div class="singleProject">
  <div class="container">
    <div class="row">

      <div class="span8">
        <div class="comments">
          <h5>
            <span class="lft"></span>
            <span class="mid">Frequently Asked Questions</span>
            <span class="rt"></span>
          </h5>
        </div>
        <div class="acc1">
          <div class="accordion" id="accordion2">
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle active" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                  Why should I get involved now?
                </a>
              </div>
              <div id="collapseTwo" class="accordion-body collapse in">
                <div class="accordion-inner">
                  <p>If you want to have the greatest say in what Outer Powell Boulevard looks like in 20 years, now is the best time to be involved. The Outer Powell Transportation Safety Project will be comparing options and trade-offs to make the road safer for
                    everyone. There will be many decisions to make, such as how wide sidewalks and bike lanes should be, where turn lanes and medians should go, whether certain trees should be removed or lighting added for better visibility at night, where bus stops
                    should be placed, etc.</p>
                  <p>It’s a long list! Your voice matters now because in later phases of the project, it is often too late to comment on these details – they have already been decided. By getting involved in the Outer Powell Transportation Safety Project now, you will
                    have the best opportunity to influence the project in the future.</p>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                  How is this project different from the prior Conceptual Design Plan?
                </a>
              </div>
              <div id="collapseOne" class="accordion-body collapse">
                <div class="accordion-inner">
                  <p>The
                    <a href="http://www.portlandoregon.gov/transportation/53084" target="_blank">2010-2013 Outer Powell Boulevard Conceptual Design Plan</a>, led by the City of Portland and ODOT, looked at the expected needs of drivers, people who walk, ride their bike, or take the bus along Outer Powell Boulevard during the next 20 years.</p>
                  <p>One of the plan’s recommendations was to widen the road to create three traffic lanes (one in each direction plus a center turn lane) for much of the corridor from I-205 to SE 174th Ave. Other suggested improvements included adding sidewalks, curb
                    ramps, pedestrian crossings, storm water facilities, and better, safer connections to transit, housing, and shopping. The plan was adopted by the Portland City Council in June 2012, with the final report complete in February 2013.</p>
                  <p>The current Outer Powell Transportation Safety Project, being led by ODOT, picks up where the prior study left off. It will refine the previous road design and study in much greater detail the locations of improvements and potential impacts on
                    corridor users, property owners and the community. Public involvement will play a big role in this process.</p>
                  <p>The work currently under way will help ODOT and it partners find future funding for final design and eventually, construction.</p>

                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                  How will project decisions be made?
                </a>
              </div>
              <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner">
                  <p>Several committees will be making decisions for the Outer Powell Transportation Safety Project. A Community Advisory Group, representing people who live, work and own property along the corridor, is an important part of project decision making and
                    represents the face of the community in project planning. Members of the Community Advisory Group reflect the diverse population of outer East Portland. The committee will meet every other month to discuss the project and make recommendations to the
                    Decision Committee.</p>
                  <p>The Decision Committee has representatives of ODOT, Federal Highway Administration, City of Portland, Metro, TriMet and the Oregon Legislature. This committee also includes the Chair of the Community Advisory Group as a voting member. The Decision
                    Committee will consider the recommendations of the Community Advisory Group in the selection of a preferred alternative and an agreement on next steps for the project.</p>
                  <p>Providing input to the Community Advisory Group and Decision Committee will be the HDR consultant team and ODOT staff as well as an Agency Working Group of ODOT agency partners.</p>

                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                  How much will this cost?
                </a>
              </div>
              <div id="collapseFour" class="accordion-body collapse">
                <div class="accordion-inner">
                  <p>ODOT is investing approximately $3 million in the Outer Powell Transportation Safety Project. This funding will complete the planning, environmental study and some design work for the corridor. This work will be finished in late 2016 and will lay
                    the ground work for additional project funding. Next steps will include a final design phase and eventually, construction.</p>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                  What improvements were made in 2013?
                </a>
              </div>
              <div id="collapseFive" class="accordion-body collapse">
                <div class="accordion-inner">
                  <p>The 2013 Outer Powell Safety Improvements Project was an $8 million project to help improve safety on the corridor immediately. These improvements have been completed:</p>
                  <ul>
                    <li>Widening the road’s shoulders for pedestrians and bicyclists.</li>
                    <li>Adding a raised bumpy strip along the edge of the vehicle lanes — known as rumble strips — to warn drivers if they cross into the shoulder.</li>
                    <li>Adding 64 curb cuts at intersections to provide better access for pedestrians, wheelchairs and strollers.
                    </li>
                    <li>Installing new pedestrian crosswalks with rapid-flash beacons at Southeast 119th, 141st and 156th Avenue intersections with Powell.</li>
                    <li>Adding new sensors at all eight main intersections on Outer Powell that detect when someone is running a red light, which triggers a delay in green lights for the cross traffic and helps avoid T-boning collisions.</li>
                    <li>Adding sensors at those eight intersections that make it easier for police to know when people run a red light, and flag them down to issue tickets.
                    <li>Adding four signs that flash motorists’ speed, two in each direction.</li>
                  </ul>
                  <p>ODOT also invested significant funding for a Transportation Safety Initiative along Outer Powell: “Slow Down, Don’t Go Around” consisting of billboard signs, window clings and lawn signs. Funding was provided by ODOT to collaborate with the
                    Portland Police Bureau, Multnomah County Sheriff’s Office and Oregon State Police to provide safety and work zone enforcement before, during and after the project was complete.</p>

                </div>
              </div>
            </div>

            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
                  What improvements were made in 2014?
                </a>
              </div>
              <div id="collapseSix" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul>
                    <li>ODOT installed two traffic separators at SE 122nd and Powell and re-signed and configured parking on the south side of SE Powell from SE 123rd to SE 125th to prevent parked cars from blocking the areas designated for bicyclists and pedestrians.</li>
                    <li>A new pedestrian crosswalk with rapid-flash beacon was installed at the SE 168th Avenue intersection with Powell.</li>
                  </ul>

                </div>
              </div>
            </div>

            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
                  What outreach is being done as part of this project?
                </a>
              </div>
              <div id="collapseSeven" class="accordion-body collapse">
                <div class="accordion-inner">
                  <p>A lot of community outreach will happen during the Outer Powell Transportation Safety Project. ODOT is committed to ensuring that people who don’t typically engage in public planning processes have a voice. Activities so far have included:</p>
                  <ul>
                    <li>Fifteen interviews with individuals and organizations that work with people who speak Russian, Vietnamese, Chinese, Spanish and other languages to help inform and guide our public engagement with these populations to ensure we are involving
                      everyone in this project.</li>
                    <li>Seven focus groups with faith-based and social service organizations working along and near Outer Powell.</li>
                    <li>Individual Outer Powell Community Walks spoken in Chinese, Russian, and Spanish, Vietnamese, and English.</li>
                    <li>An Outer Powell Safety Project Community Advisory Group, representing diverse organizations and interests, has been established.</li>
                    <li>Hosted two open house events.</li>
                  </ul>

                </div>
              </div>
            </div>

            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseEight">
                  When might we see construction happening?
                </a>
              </div>
              <div id="collapseEight" class="accordion-body collapse">
                <div class="accordion-inner">
                  <!-- <p>
                    The current planning effort will be complete in late 2016. Dates beyond that depend on additional funding for the project. ODOT estimates costs of $10 million to complete the full design of Outer Powell to get the corridor ‘construction ready.’ We
                    have $3 million to begin this work and still need to identify $7 million to complete the design work. Once design is complete, typically a 2-year process, we estimate it will cost between $65 million to $75 million to construct the improvements. This
                    is a planning-level estimate. Funding for construction has not yet been identified.
                  </p> -->
                  <p>
                    The current planning effort for the corridor will be complete in 2016. The state Legislature has provided $17 million to design and construct the SE 122nd Avenue to SE 136th Avenue section of Powell. Final design work is expected to begin in spring 2016.
                    Construction is expected in 2019. Additional design and construction phases beyond that depend on additional funding for the project.
                  </p>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <div class="span4">
        <div class="acc1">
          <div class="accordion" id="accordion3">
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle active" data-toggle="collapse" data-parent="#accordion3" href="#collapseTen">
                  Project Documents
                </a>
              </div>
              <div id="collapseTen" class="accordion-body collapse in">
                <div class="accordion-inner">
                  <ul class="documents">
                    <li>
                      <a href="doc/infographic.pdf" target="_blank">Infographic</a>
                    </li>
                    <li>
                      <a href="doc/conceptual-design-plan.pdf" target="_blank">2010-2013 Conceptual Design Plan</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_DRAFT_API_12.4.15_LoRes.pdf" target="_blank">Draft Area of Potential Impact</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_cross_sections_2015-08-11.pdf" target="_blank">Proposed Cross Sections</a>
                    </li>
                    <li>
                      <a href="/tiger">TIGER Grant Application Materials</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_ES_working_2016-06-06_2nd_version.pdf" target="_blank">Draft Environmental Summary Report</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_Multimodal_Options_Memo.pdf" target="_blank">Multimodal Options Memo</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_Boards_03172017.pdf" target="_blank">Open House Display Boards &mdash; March 2017</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_ProjectMap_03172017.pdf" target="_blank">Project Map &mdash; March 2017</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseNine">
                  Fact Sheets
                </a>
              </div>
              <div id="collapseNine" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul class="documents">
                    <li>
                      <a href="doc/factsheet-english.pdf" target="_blank">Factsheet (English)</a>
                    </li>
                    <li>
                      <a href="doc/factsheet-russian.pdf" target="_blank">Factsheet (Russian)</a>
                    </li>
                    <li>
                      <a href="doc/factsheet-vietnamese.pdf" target="_blank">Factsheet (Vietnamese)</a>
                    </li>
                    <li>
                      <a href="doc/factsheet-spanish.pdf" target="_blank">Factsheet (Spanish)</a>
                    </li>
                    <li>
                      <a href="doc/factsheet-chinese.pdf" target="_blank">Factsheet (Chinese)</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseEleven">
                  Committee Summaries
                </a>
              </div>
              <div id="collapseEleven" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul class="documents">
                    <li>
                      <a href="doc/Community_Advisory_Group_Mtg_1_2014_12_03.pdf" target="_blank">Community Advisory Group Meeting #1</a>
                    </li>
                    <li>
                      <a href="doc/Community_Advisory_Group_Mtg_2_2015_01_26.pdf" target="_blank">Community Advisory Group Meeting #2</a>
                    </li>
                    <li>
                      <a href="doc/Community_Advisory_Group_Mtg_3_2015_03_18.pdf" target="_blank">Community Advisory Group Meeting #3</a>
                    </li>
                    <li>
                      <a href="doc/Community_Advisory_Group_Mtg_4_2015_06_08.pdf" target="_blank">Community Advisory Group Meeting #4</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_CAG_Meeting_5_Presentation.pdf" target="_blank">Community Advisory Group Meeting #5</a>
                    </li>
                    <li>
                      <a href="doc/MM_OPTSP_CAG_Mtg_6_2015_12_02.pdf" target="_blank">Community Advisory Group Meeting #6</a>
                    </li>
                    <li>
                      <a href="doc/MM_OPTSP_CAG_Mtg_7_2016_06_15.pdf" target="_blank">Community Advisory Group Meeting #7</a>
                    </li>
                    <li>
                      <a href="doc/MM_OPTSP_CAG_Mtg_8_2017-03-20.pdf" target="_blank">Community Advisory Group Meeting #8</a>
                    </li>
                    <li>
                      <a href="doc/Decision_Committee_Mtg_1_2014_12_11.pdf" target="_blank">Decision Committee Meeting #1</a>
                    </li>
                    <li>
                      <a href="doc/Decision_Committee_Mtg_2_2015_03_31.pdf" target="_blank">Decision Committee Meeting #2</a>
                    </li>
                    <li>
                      <a href="doc/Decision_Committee_Mtg_3_2015_06_23.pdf" target="_blank">Decision Committee Meeting #3</a>
                    </li>
                    <li>
                      <a href="doc/MM_OPTSP_Decision_Committee_Mtg_4_2015_12_08.pdf" target="_blank">Decision Committee Meeting #4</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseTwelve">
                  Public Outreach Summaries
                </a>
              </div>
              <div id="collapseTwelve" class="accordion-body collapse">
                <div class="accordion-inner">
                  <ul class="documents">
                    <li>
                      <a href="doc/Open_House_Summary_2014_12_09.pdf" target="_blank">Open House #1 Summary</a>
                    </li>
                    <li>
                      <a href="doc/Open_House_Summary_2015_03_09.pdf" target="_blank">Open House #2 Summary</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_Open_House_Summary_2015_09_16_Final.pdf" target="_blank">Open House #3 Summary</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_Community_Site_Walks_Summary.pdf" target="_blank">Community Site Walks Summary</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_Focus_Groups_Summary.pdf" target="_blank">Focus Groups Summary</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_Bus_Canvassing_Summary.pdf" target="_blank">Bus Canvassing Summary</a>
                    </li>
                    <li>
                      <a href="doc/OPTSP_Bike_Ride_Summary_2015_08_01.pdf" target="_blank">Bike Ride Summary</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include 'footer.php'; ?>
