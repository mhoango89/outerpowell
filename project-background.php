<?php include 'header.php'; ?>



<div id="titleRow">
	<div class="container">
		<h1 class="work">
			<span class="lft"></span><span class="mid">About the project</span><span class="rt"></span>
		</h1>
	</div>
</div>
<div class="container">
	<img src="img/about-top-banner.jpg" alt="" />
</div>
<div class="container">
	<p class="pop-more">
		The Oregon Department of Transportation (ODOT) will construct roadway improvements on Powell Boulevard
		(US Highway 26) from SE 122nd Avenue to SE 136th Avenue. This project will address the high rate of
		collisions on Outer Powell for the past several years. The purpose of this project is to increase safety
		for people who walk, drive, take the bus and ride bikes, making Outer Powell a safer place for everyone.
	</p>
</div>

<div class="container">
	<h3 class="no-bottom-margin">WHAT KINDS OF IMPROVEMENTS CAN BE ANTICIPATED?</h3>
	<img src="img/Factsheet map v2.png" alt="" />
</div>
<div id="aboutUsRow2">
	<div class="container">
		<div class="row">
			<div class="span4">
				<ul class="thumbnails corner">
					<li>
						<div class="thumbnail nolink"><span class="topCorner"></span>
							<a href="img/CrossSection.jpg" class="colorUp">
								<img class="shrinkimageie" src="img/CrossSection.jpg" alt=" ">
							</a><span class="bottomCorner"></span></div>
					</li>
				</ul>
			</div>
			<div class="span8">
				<p class="pop-more">
					When the Outer Powell Transportation Safety Project is finished, people will see and experience new roadway
					improvements including:
				</p>
				<ul class="pop-more">
					<li>Crosswalks so people can cross the road safely</li>
					<li>Sidewalks for pedestrians and people who use wheel chairs</li>
					<li>Dedicated bicycle facilities for cyclists</li>
					<li>Center turn lanes so that cars, buses and trucks can move through the corridor more safely and efficiently</li>
					<li>Storm drains to keep water from pooling on the road when it rains</li>
					<li>Lighting for improved visibility</li>
					<li>Rapid Flash Beacons (which alert drivers that people are crossing the street with flashing lights) to increase
						safety at the busiest crosswalks</li>
					<li>Improved signals for better safety and traffic flow at intersections</li>
				</ul>
				<!-- <a href="schedule.php" class="btn red-btn">View a project schedule</a>&nbsp;&nbsp;&nbsp;<a href="resources.php" class="btn red-btn">View a factsheet</a> -->
			</div>


		</div>
	</div>
</div>

<div class="container"></div>

<!--
<div id="aboutUsRow2">
	<div class="container">
		<div class="row">
			<div class="span4">
				<ul class="thumbnails corner">
					<li>
						<div class="thumbnail nolink"><span class="topCorner"></span>
							<a href="img/content/infographic.jpg" class="colorUp">
								<img class="shrinkimageie" src="img/content/infographic.jpg" alt=" ">
							</a><span class="bottomCorner"></span></div>
					</li>

				</ul>
			</div>
			<div class="span8">
				<p>
					The Oregon Department of Transportation (ODOT) is beginning planning, environmental, and initial design work for a four-mile stretch of Powell Boulevard (US Highway 26) in East Portland. The study will propose safety and corridor improvements on SE Powell
					Boulevard from east of I-205 to the Portland/Gresham city limits at SE 174th Avenue. ODOT will be preparing an environmental assessment that will examine whether or not the project has significant impacts. This involves extensive public involvement
					and more detailed information about potential project impacts consistent with federal law. The entire planning process is expected to be completed in early 2016.
				</p>
				<a href="schedule.php" class="btn red-btn">View a project schedule</a>&nbsp;&nbsp;&nbsp;<a href="resources.php" class="btn red-btn">View a factsheet</a>
			</div>
		</div>
	</div>
</div>
-->

<div class="container">
	<h3 class="no-bottom-margin">WHEN?</h3>
	<p>
		The design work of the safety improvements will wrap up in fall 2018. Early tree removal and utility relocations will
		begin in November 2018. Roadway construction will begin in spring 2019. Once roadway improvements are complete,
		construction crews will apply landscaping
	</p>
	<img src="img/Outer_Powell_Timeline4web_03142019.png" alt="">
	<!-- <img src="img/Timeline.jpg" alt="" /> -->
	<br /><br />
	<p class="pop-more">
		The design phase began in summer 2016 and will last through summer 2018.
		Construction is anticipated to begin in 2019 and last about two years.
	</p>
</div>



<div class="container">
	<h3 class="no-bottom-margin">HOW WILL THIS PROJECT AFFECT YOU?</h3>

	<p class="pop-more">
		When it’s finished, this project will help you get around outer SE Powell more safely.
		But construction will bring temporary inconveniences like traffic delays, noise and detours.
		In some cases, changes will need to be made to driveways or the area where property meets the road.
		If you live, work or own property on Powell Boulevard between SE 122nd and SE 136th Avenues,
		your driveway or frontage may need changes. If changes are needed, an ODOT team member will contact you.
	</p>
</div>

<div class="container">
	<h3 class="no-bottom-margin">Future Phases of the Project – SE 99th to SE 176th </h3>

	<p class="pop-more">
		Through the passage of House Bill 2017, the Oregon Legislature delegated funds for improvements on the remainder of the 
		Outer Powell Transportation Safety Project, from approximately SE 99th Avenue to SE 176th Avenue. With this funding, ODOT 
		will begin designing improvements to these other areas of Outer Powell. Design will be consistent with the section from 
		SE 122nd to SE 136th Avenues. The project team will continue to engage the public throughout future phases of the project.
	</p>
	<br /><br/>
	<a href="contact.php" class="btn red-btn">CONTACT US</a>
</div>

<div class="container">
	<br /><br />
</div>



<div id="ourStatistics" class="green-row">
	<div class="container">
		<h2>
			<!-- <span class="lft"></span><span class="mid">Project need: by the numbers</span><span class="rt"></span> -->
			<span class="lft"></span><span class="mid">TO THE POINT</span><span class="rt"></span>
		</h2>

		<div class="row">
			<div class="project">
				<div class="span3">

					<h3><i class="fa fa-map-signs" aria-hidden="true" style="color:rgb(255, 255, 255);font-size:3.5em;"></i> <span
						 style="font-size:20px;vertical-align:super;display: inline-block;">&nbsp Where</span></h3>
					<p>Powell Boulevard from SE 122nd to SE 136th Avenues</p>
				</div>

				<div class="span3">

					<h3><i class="fa fa-question-circle-o" aria-hidden="true" style="color:rgb(255, 255, 255);font-size:3.5em;"></i>
						<span style="font-size:20px;vertical-align:super;display: inline-block;">&nbsp Why</span></h3>
					<p>Studies have shown that Outer Powell has experienced an increasing number of collisions – some of the highest
						in the state.</p>
				</div>

				<div class="span3">

					<h3><i class="fa fa-info-circle" aria-hidden="true" style="color:rgb(255, 255, 255);font-size:3.5em;"></i> <span
						 style="font-size:20px;vertical-align:super;display: inline-block;">&nbsp What</span></h3>
					<p>Multi-modal safety improvements including sidewalks, bike lanes, cross walks and center turn lanes will be
						constructed.</p>
				</div>

				<div class="span3">

					<h3><i class="fa fa-clock-o" aria-hidden="true" style="color:rgb(255, 255, 255);font-size:3.5em;"></i> <span style="font-size:20px;vertical-align:super;display: inline-block;">&nbsp
							When</span></h3>
					<p><b>Design:</b> Summer 2016 to Summer 2018</p>
					<p><b>Construction:</b> Early 2019 (will last approximately 2 years)</p>
				</div>





			</div>
		</div>
	</div>
</div>


<div id="aboutUsRow3">
	<div class="container">

		<div class="row">
			<div class="span4">
				<h3>HOW DID WE GET HERE?</h3>

				<!-- <p>
					The project follows the 2013 Outer Powell Boulevard Conceptual Design Plan, a City of Portland and ODOT project. This study identified the need for corridor improvements, including sidewalks, curb ramps, pedestrian crossings, stormwater facilities
					and better, safer connections to transit, housing, and shopping. It also proposed adding a new center lane to provide a safe area for vehicles to turn through the corridor. As a first step, ODOT began implementing some elements of the concept plan
					in 2013. This work included paving, striping, and adding four new signalized pedestrian crossings to help improve corridor safety immediately while the larger planning effort begins.
				</p> -->
				<p>
					From 2014 to 2016, ODOT conducted planning, environmental, and initial design work for the four-mile stretch of
					Powell
					Boulevard from east of I-205 from approximately SE 99th Avenue to the Portland/Gresham city limits at SE 176th
					Avenue.
					In fall 2015, the state Legislature approved $17 million to design and construct the SE 122nd to SE 136th Avenue
					section
					of Powell (which is why this project is underway!). ODOT is dedicated to pursuing more funding opportunities so
					the entire corridor can receive these safety improvements.
					Prior to the planning and environmental phase, a preliminary study was performed as a joint effort between ODOT
					and the Portland Bureau of Transportation.
					From this study, the <a href="http://www.portlandoregon.gov/transportation/53084" target="_blank">2010-2013 Outer
						Powell Boulevard Conceptual Design Plan</a> was developed.

				</p>
				<!-- <h5><a href="schedule.php">View project schedule</a></h5> -->

			</div>
			<div class="span4">
				<h3>WHY IS THIS PROJECT NEEDED?</h3>
				<p>
					Fatal and serious injury collisions affect the lives of many Oregonians every year. In fact, since 2003 the
					intersection at
					Powell Boulevard and SE 122nd Avenue has been one of the State’s top 5 percent sites for the number and severity
					of crashes.
					This is why the section of Powell Boulevard from SE 122nd Avenue and SE 136th Avenue has been prioritized for
					roadway safety
					improvements. Sidewalks, dedicated bike facilities, cross walks and center turn lanes have been identified to help
					significantly
					reduce the number and severity of collisions in this area.
				</p>
				<!-- <p>Outer Powell Boulevard is as diverse as the neighborhoods that it passes through. It is a state highway serving truckers and through travelers, a major roadway serving commuters, and a local street serving neighborhoods. There are no sidewalks or
					buffered bike lanes along most of the corridor and rear-end collisions between vehicles are common. The narrow width of the two-lane road combined with the large number of motorists, pedestrians, and cyclists increases the risk of collisions. In
					fact, the intersection of Powell Boulevard and SE 122nd is the #1 location for the highest number and severity of crashes in the state.</p>
				<h5><a href="img/content/infographic.jpg" class="colorUp">View infographic</a></h5> -->
			</div>
			<div class="span4">
				<h3>Who is involved?</h3>
				<p>
					ODOT is leading the Outer Powell Transportation Safety Project with support from a consultant team led by HDR
					Engineering. A
					Community Advisory Group and other technical and policy committees met throughout the planning and environmental
					phase to
					guide the project. The Community Advisory Group will continue to meet and serve as community liaisons. Also,
					public meetings
					and community activities will continue to be held. Updates will be posted to this website as the project
					progresses. Stay
					tuned for project updates and ways to be involved.
				</p>
				<!-- <p>ODOT is leading the Outer Powell Transportation Safety Project with consultant support from a team led by HDR Engineering. For a successful project, your participation matters! ODOT is committed to including everyone in an open public process. A Community
					Advisory Group and other technical and policy committees are meeting to guide the project. Also, public meetings, workshops, and site visits will be held, and this website has been created. Project information has been translated into multiple languages
					and specific cultural groups are being contacted to ensure all voices are heard.</p> -->

			</div>

		</div>
	</div>
</div>

<!-- <div id="ourClients">
	<div class="container">

			<h2>
				<span class="lft"></span><span class="mid">Some of our clients</span><span class="rt"></span>
			</h2>

			<div id="sliderRow">
				<div class="container">
					<div class="logosSlider">
						<div id="myCarousel3" class="carousel slide">
							<!-- Carousel items -->
<!-- <div class="carousel-inner">
								<div class="active item">
									<div class="box">
                    <img src="img/content/logo1.png" alt=""><img src="img/content/logo2.png" alt=""><img src="img/content/logo3.png" alt=""><img src="img/content/logo4.png" alt=""><img src="img/content/logo5.png" alt=""><img src="img/content/logo6.png" alt="">
									</div>
								</div>
								<div class="item">
									<div class="box">
                    <img src="img/content/logo3.png" alt=""><img src="img/content/logo1.png" alt=""><img src="img/content/logo2.png" alt=""><img src="img/content/logo4.png" alt=""><img src="img/content/logo5.png" alt=""><img src="img/content/logo6.png" alt="">
									</div>
								</div>
								<div class="item">
									<div class="box">
                    <img src="img/content/logo2.png" alt=""><img src="img/content/logo1.png" alt=""><img src="img/content/logo3.png" alt=""><img src="img/content/logo4.png" alt=""><img src="img/content/logo5.png" alt=""><img src="img/content/logo6.png" alt="">
									</div>
								</div>
							</div>
							<!-- Carousel nav -->
<!-- <a class="carousel-control left" href="#myCarousel3" data-slide="prev"></a>
							<a class="carousel-control right" href="#myCarousel3" data-slide="next"></a>
						</div>
					</div>
				</div>
			</div>

	</div>
</div> -->




<!--<div id="meetTheTeam">
	<div class="container">
		<h2>
			<span class="lft"></span><span class="mid">Meet the team</span><span class="rt"></span>
		</h2>

		<div class="row">
			<div class="person">
				<div class="span3">
					<ul class="thumbnails">
						<li class="span3">
							<div class="thumbnail nolink">
								<span class="topCorner"></span>
                <img src="img/content/about-team-1.jpg" alt="">
								<span class="bottomCorner"></span>
							</div>
						</li>
					</ul>

				</div>
				<div class="span3">
					<h3>William Lewis</h3>
					<span>Creative Director</span><br>
					<a href="#">@williamlewis</a><br>
					<a href="#">william.lewis@quartum.com</a>

				</div>

				<div class="span6">
					<p>Integer et mauris neque. Morbi pellentesque tempor ornare. Morbi vehicula massa id lacus gravida tincidunt. Donec eu tempor eros. Praesent ipsum elit, blandit nec venenatis ac, semper non tellus. Proin
						risus sem, mattis vel vestibulum eget, consequat vitae felis. Nulla ac neque tortor. Pellentesque ullamcorper lacinia dui. Duis sagittis posuere sem, ac lacinia tortor congue sed. In hendrerit, neque
						sit amet dapibus hendrerit, elit dolor lobortis nunc, vitae vestibulum nunc tortor vehicula nibh. Nunc et tellus nisi.</p>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="person">
				<div class="span3">
					<ul class="thumbnails">
						<li class="span3">
							<div class="thumbnail nolink">
								<span class="topCorner"></span>
                <img src="img/content/about-team-2.jpg" alt="">
								<span class="bottomCorner"></span>
							</div>
						</li>
					</ul>

				</div>
				<div class="span3">
					<h3>Rick Spreengfield</h3>
					<span>Lead Designer</span><br>
					<a href="#">@rickspreengfield</a><br>
					<a href="#">rick.spreengfield@quartum.com</a>
				</div>

				<div class="span6">
					<p>Cras risus sem, semper eu tempor a, fringilla ut mauris. Nam erat tellus, elementum sed vestibulum eu, rhoncus non lorem. Vestibulum tempor libero et quam mollis consequat. Sed velit lacus, consequat
						eu
						auctor id, pellentesque at risus. Donec hendrerit placerat accumsan.</p>

				</div>
			</div>
		</div>
	</div>
</div> -->


<?php include 'footer.php'; ?>