<?php include 'header.php'; ?>







<div id="titleRow">
  <div class="container">
    <h1 class="work">
      <span class="lft"></span><span class="mid">Construction Impacts </span><span class="rt"></span>
    </h1>
  </div>
</div>


<div class="container">
  <p class="pop-more">
  During construction, two-way traffic will be maintained on Powell Boulevard with some intermittent lane closures 
  at night time. A marked pedestrian path and striped bike lane will be maintained in both directions. All driveway 
  access will be maintained or temporarily accommodated.
  </p>
<br/>
 <img class="construction-jumbotron" src="./img/Outer_Powell_Timeline4web_03142019.png" alt="">
 <br/><br/>
 <img class="construction-jumbotron" src="./img/Outer_Powell_Staging4web_03142019.png" alt="">

<!--
  <img class="construction-jumbotron" src="./img/construction/overview.png" alt="">

  <br>
  <div class="construction-wrapper">
    <img class="text-only" src="./img/construction/stage1_text_only.png" alt="">
    <img class="image-only" src="./img/construction/stage1.png" alt="">
  </div>

  <div class="construction-wrapper">
    <img class="text-only" src="./img/construction/stage2_text_only.png" alt="">
    <img class="image-only" src="./img/construction/stage2.png" alt="">
  </div>

  <div class="construction-wrapper">
    <img class="text-only" src="./img/construction/stage3_text_only.png" alt="">
    <img class="image-only" src="./img/construction/stage3.png" alt="">
  </div>
</div>
-->

<br/><br/>


<div id="traffic" class="container">
  <h3 class="no-bottom-margin">Traffic </h3>
  <img class="const-header-icon" src="./img/construction-icons/traffic.png" alt="">
  <br>
  <p class="pop-more">
    Two-way traffic will be maintained on SE Powell Boulevard during the day. However, occasional single lane closures 
    at nighttime may occur on SE Powell Boulevard and at the intersections of SE 122nd and SE 136th Avenues. 
    Flaggers will be in place to direct traffic. Some daytime and nighttime closures of minor cross streets may occur.
  </p>
  <p>
    On-street parking on Powell will be closed, with a 48-hour notice and signage warning of the closure.
  </p>
  <p>
    To keep travelers informed about construction, ODOT will use portable electronic message signs. Static signs at 
    the beginning and end of the work zones will also alert travelers of construction ahead.
  </p>
</div>


<div id="driveway" class="container">
  <h3 class="no-bottom-margin">Driveway Access </h3>
  <img class="const-header-icon" src="./img/construction-icons/driveway.png" alt="">
  <br>
  <p class="pop-more">
    All driveway access will be maintained or temporarily accommodated during construction. Some temporary driveways
    will be provided at critical locations to maintain continuous access. To help motorists accessing businesses, blue
    tubular markers will be used to show the temporary access point. Garbage pickup, mail service, and other delivery
    vehicles (such as UPS or FedEx) will also be accommodated during construction. Lane closures may occur in these
    areas with flagging to continue the flow of traffic as needed. In some cases, public side streets will be closed to
    through traffic but will allow local access.
  </p>

</div>



<div id="bike-ped" class="container">
  <h3 class="no-bottom-margin">Bike & Pedestrian </h3>
  <img class="const-header-icon" src="./img/construction-icons/bike-ped.png" alt="">
  <br>
  <p class="pop-more">
    A marked bike path and a pedestrian path will be maintained in both directions, with the exception of some bike 
    lane closures on weekends and at night time. During these closures, bicyclists will be directed to share the road
    with other vehicles. In these cases, signage will be provided alerting travelers that the bike lane has ended, as
    well as signage stating “Bikes on Roadway.”
  </p>
  <p>
    Pedestrian routes meeting ADA requirements will provide access to adjacent properties during construction. Along
    this path, sections of sidewalk may be closed and pedestrians diverted to a temporary pedestrian-accessible route
    in the roadway shoulder.
  </p>
  <p>
    In all cases, a barrier will be placed between the pedestrian path and any work on the same side of the road. A
    striped bike lane will also be maintained, similar to what you see on Powell today.
  </p>

</div>



<div id="bus" class="container">
  <h3 class="no-bottom-margin">Bus</h3>
  <img class="const-header-icon" src="./img/construction-icons/bus.png" alt="">
  <br>
  <p class="pop-more">
    TriMet bus routes will remain operational during construction, although bus stops within the project area may be
    relocated or closed temporarily. ODOT will work with TriMet to coordinate as we get closer to construction.
    Emergency vehicles and mobility will be accommodated throughout the project area during construction.
  </p>

</div>


<div id="noise" class="container">
  <h3 class="no-bottom-margin">Noise </h3>
  <img class="const-header-icon" src="./img/construction-icons/noise.png" alt="">
  <br>
  <p class="pop-more">
    Residences and businesses adjacent to the project may experience construction noise throughout construction. Peak
    noise levels will range from 89 decibels to 96 decibels for those residences closest to the street, meaning that
    the level of noise is similar to that of a vacuum cleaner or motorcycle. This noise will be short term and mostly 
    done during daytime construction hours.
  </p>
  <p>
    Limited nighttime and weekend work, including utility work, tree removal and paving, will result in some noise as
    well. ODOT has obtained required noise variances in order to complete the work. ODOT and the contractor will work
    to minimize the noise as much as possible, but there will be unavoidable noise.
  </p>
  <p>
    For concerns related to noise, use the 24-hour noise hotline: 503-294-1321
  </p>

</div>




<div id="tree-removal" class="container">
  <h3 class="no-bottom-margin">Tree Removal & Utility Relocations </h3>
  <img class="const-header-icon" src="./img/construction-icons/tree-removal.png" alt="">
  <br>
  <p class="pop-more">
    From winter through spring 2019, utility crews will be at work day and night to move power, communications, water and 
    gas lines, including overhead and underground utilities. During this time, there will be occasional power outages with 
    advance notice to affected properties. People can also expect intermittent traffic delays and flaggers, construction 
    noise, bright lights, temporary sidewalk and bike lane detours and no on-street parking.
  </p>
</div>








<!-- 

<div class="container">
  <img src="img/about-top-banner.jpg" alt="" />
</div>
<div class="container">
  <p class="pop-more">
    The Oregon Department of Transportation (ODOT) will construct roadway improvements on Powell Boulevard
    (US Highway 26) from SE 122nd Avenue to SE 136th Avenue. This project will address the high rate of
    collisions on Outer Powell for the past several years. The purpose of this project is to increase safety
    for people who walk, drive, take the bus and ride bikes, making Outer Powell a safer place for everyone.
  </p>
</div>

<div class="container">
  <h3 class="no-bottom-margin">WHAT KINDS OF IMPROVEMENTS CAN BE ANTICIPATED?</h3>
  <img src="img/Factsheet-map.jpg" alt="" />
</div>
<div id="aboutUsRow2">
  <div class="container">
    <div class="row">
      <div class="span4">
        <ul class="thumbnails corner">
          <li>
            <div class="thumbnail nolink"><span class="topCorner"></span>
              <a href="img/CrossSection.jpg" class="colorUp">
                <img class="shrinkimageie" src="img/CrossSection.jpg" alt=" ">
              </a><span class="bottomCorner"></span></div>
          </li>
        </ul>
      </div>
      <div class="span8">
        <p class="pop-more">
          When the Outer Powell Transportation Safety Project is finished, people will see and experience new roadway
          improvements including:
        </p>
        <ul class="pop-more">
          <li>Crosswalks so people can cross the road safely</li>
          <li>Sidewalks for pedestrians and people who use wheel chairs</li>
          <li>Dedicated bicycle facilities for cyclists</li>
          <li>Center turn lanes so that cars, buses and trucks can move through the corridor more safely and
            efficiently</li>
          <li>Storm drains to keep water from pooling on the road when it rains</li>
          <li>Lighting for improved visibility</li>
          <li>Rapid Flash Beacons (which alert drivers that people are crossing the street with flashing lights) to
            increase safety at the busiest crosswalks</li>
          <li>Improved signals for better safety and traffic flow at intersections</li>
        </ul>
      </div>


    </div>
  </div>
</div>

<div class="container"></div>

<div class="container">
  <h3 class="no-bottom-margin">WHEN?</h3>

  <img src="img/Timeline.jpg" alt="" />
  <br /><br />
  <p class="pop-more">
    The design phase began in summer 2016 and will last through summer 2018.
    Construction is anticipated to begin in 2019 and last about two years.
  </p>
</div>



<div class="container">
  <h3 class="no-bottom-margin">HOW WILL THIS PROJECT AFFECT YOU?</h3>

  <p class="pop-more">
    When it’s finished, this project will help you get around outer SE Powell more safely.
    But construction will bring temporary inconveniences like traffic delays, noise and detours.
    In some cases, changes will need to be made to driveways or the area where property meets the road.
    If you live, work or own property on Powell Boulevard between SE 122nd and SE 136th Avenues,
    your driveway or frontage may need changes. If changes are needed, an ODOT team member will contact you.
  </p>
  <a href="contact.php" class="btn red-btn">CONTACT US</a>
</div>

<div class="container">
  <br /><br />
</div>



<div id="ourStatistics" class="green-row">
  <div class="container">
    <h2>
      <span class="lft"></span><span class="mid">TO THE POINT</span><span class="rt"></span>
    </h2>

    <div class="row">
      <div class="project">
        <div class="span3">

          <h3><i class="fa fa-map-signs" aria-hidden="true" style="color:rgb(255, 255, 255);font-size:3.5em;"></i>
            <span style="font-size:20px;vertical-align:super;display: inline-block;">&nbsp Where</span></h3>
          <p>Powell Boulevard from SE 122nd to SE 136th Avenues</p>
        </div>

        <div class="span3">

          <h3><i class="fa fa-question-circle-o" aria-hidden="true" style="color:rgb(255, 255, 255);font-size:3.5em;"></i>
            <span style="font-size:20px;vertical-align:super;display: inline-block;">&nbsp Why</span></h3>
          <p>Studies have shown that Outer Powell has experienced an increasing number of collisions – some of the
            highest in the state.</p>
        </div>

        <div class="span3">

          <h3><i class="fa fa-info-circle" aria-hidden="true" style="color:rgb(255, 255, 255);font-size:3.5em;"></i>
            <span style="font-size:20px;vertical-align:super;display: inline-block;">&nbsp What</span></h3>
          <p>Multi-modal safety improvements including sidewalks, bike lanes, cross walks and center turn lanes will be
            constructed.</p>
        </div>

        <div class="span3">

          <h3><i class="fa fa-clock-o" aria-hidden="true" style="color:rgb(255, 255, 255);font-size:3.5em;"></i> <span
              style="font-size:20px;vertical-align:super;display: inline-block;">&nbsp When</span></h3>
          <p><b>Design:</b> Summer 2016 to Summer 2018</p>
          <p><b>Construction:</b> Early 2019 (will last approximately 2 years)</p>
        </div>





      </div>
    </div>
  </div>
</div>


<div id="aboutUsRow3">
  <div class="container">

    <div class="row">
      <div class="span4">
        <h3>HOW DID WE GET HERE?</h3>


        <p>
          From 2014 to 2016, ODOT conducted planning, environmental, and initial design work for the four-mile stretch
          of Powell
          Boulevard from east of I-205 from approximately SE 99th Avenue to the Portland/Gresham city limits at SE
          176th Avenue.
          In fall 2015, the state Legislature approved $17 million to design and construct the SE 122nd to SE 136th
          Avenue section
          of Powell (which is why this project is underway!). ODOT is dedicated to pursuing more funding opportunities
          so the entire corridor can receive these safety improvements.
          Prior to the planning and environmental phase, a preliminary study was performed as a joint effort between
          ODOT and the Portland Bureau of Transportation.
          From this study, the <a href="http://www.portlandoregon.gov/transportation/53084" target="_blank">2010-2013
            Outer Powell Boulevard Conceptual Design Plan</a> was developed.

        </p>

      </div>
      <div class="span4">
        <h3>WHY IS THIS PROJECT NEEDED?</h3>
        <p>
          Fatal and serious injury collisions affect the lives of many Oregonians every year. In fact, since 2003 the
          intersection at
          Powell Boulevard and SE 122nd Avenue has been one of the State’s top 5 percent sites for the number and
          severity of crashes.
          This is why the section of Powell Boulevard from SE 122nd Avenue and SE 136th Avenue has been prioritized for
          roadway safety
          improvements. Sidewalks, dedicated bike facilities, cross walks and center turn lanes have been identified to
          help significantly
          reduce the number and severity of collisions in this area.
        </p>
      </div>
      <div class="span4">
        <h3>Who is involved?</h3>
        <p>
          ODOT is leading the Outer Powell Transportation Safety Project with support from a consultant team led by HDR
          Engineering. A
          Community Advisory Group and other technical and policy committees met throughout the planning and
          environmental phase to
          guide the project. The Community Advisory Group will continue to meet and serve as community liaisons. Also,
          public meetings
          and community activities will continue to be held. Updates will be posted to this website as the project
          progresses. Stay
          tuned for project updates and ways to be involved.
        </p>
      
      </div>

    </div>
  </div>
</div>
 -->




<?php include 'footer.php'; ?>