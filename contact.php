<?php include 'header.php'; ?>

<div id="titleRow">
    <div class="container">
        <h1 class="work">
            <span class="lft"></span>
            <span class="mid">Contact the project team</span>
            <span class="rt"></span>
        </h1>
    </div>
</div>

<div class="singleProject">
    <div class="container">
        <div class="row">
            <div class="span8">
                <!--h4>Your voice matters</h4>
                <p>We want to hear from as many people as possible to determine the best ways to improve Outer Powell Boulevard.
                    Submit your online comment here.</p>
                <br /-->
                <h4>Stay connected</h4>
                <!--p>You may also use the form below to subscribe to our mailing list (Don’t worry, we won’t fill up your inbox
                    and your personal information won’t be shared with anyone.)</p-->
                <iframe src=" https://irealmccm.hdrinc.com/Comments/PIWebCommentForm.aspx?ProjectID=97" width="100%" height="600" frameborder="0"                        
                    scrolling="auto">
                </iframe>
            </div>
            <div class="span4">
                <div class="projDescription">
                    <span>
                        <strong>ODOT Project Contact</strong>
                        <br> Ellen Sweeney
                        <br /> Community Affairs Coordinator
                        <br /> 503.731.8230
                        <br />
                        <a href="mailto:ellen.sweeney@odot.state.or.us?subject=Outer Powell Safety">Email</a>
                    </span>
                    <br>
                    <br>
                    <br>

                    <span>
                        <strong>HDR Project Contact</strong>
                        <br> Cassie Davis
                        <br /> Public Involvement Coordinator
                        <br /> 503.727.3922
                        <br />
                        <a href="mailto:cassie.davis@hdrinc.com?subject=Outer Powell Safety">Email</a>
                    </span>
                    <br>
                    <br>
                    <br>

                    <!--<span><strong>Translations</strong><br>
                    Su voz importa<br>
					Vấn đề giọng nói của bạn<br>
					Ваш голос имеет значение<br>
					음성 문제<br>
					你的声音问题
                    </span><br><br>-->

                </div>

            </div>
        </div>
    </div>
</div>


<?php include 'footer.php'; ?>