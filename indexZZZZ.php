<?php include 'header.php'; ?>

<!-- End Main Menu and Logo -->
<div id="sloganRow">
    <div class="container">
        <h1></h1>
    </div>
</div>
<div id="sliderRow" style="border-width:1px">
    <div class="container">

        <div class="slider">
            <div id="quartumCarousel" class="carousel slide">
                <!-- Carousel items -->
                <div class="carousel-inner">


                    <!-- 20181025 hoa
                    <div class="active item">
                        <div class="carousel-caption carousel-caption-below-image">
                            <div class="w1 pull-left">
                                <a href="get-involved.php">
                                    <span class="title">Public Open House</span>
                                </a>
                                <span class="description">Join us for a project open house on September 27, 2018</span>
                            </div>
                            <div class="w2 pull-right">
                                <a href="get-involved.php" class="btn red-btn">LEARN MORE</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="box">
                            <img src="img/banner-Open-House-Image.jpg" alt="">
                        </div>
                        <div style="height:100px;"></div>
                    </div>
                    -->

                    
                    <div class="active item">
                        <div class="box">
                            <img src="img/content/home-usual-slider-3.jpg" alt="">
                        </div>
                        <div class="carousel-caption carousel-caption-below-image">
                            <div class="w1 pull-left">
                                <a href="project-background.php">
                                    <span class="title">What is this project?</span>
                                </a>
                                <span class="description">Stay informed about upcoming roadway changes</span>
                            </div>
                            <div class="w2 pull-right">
                                <a href="about.php" class="btn red-btn">LEARN MORE</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div style="height:100px;"></div>
                    </div>

                    <div class="item">
                        <div class="box">
                            <img src="img/Outer_Powell_GBC_Flyer_Draft1.3-top-half.jpg" alt="">
                        </div>
                        <div class="carousel-caption carousel-caption-below-image">
                            <div class="w1 pull-left">
                                <a href="construction.php">
                                    <span class="title">GROUND BREAKING CEREMONY</span>
                                </a>
                            </div>
                            <!--div class="w2 pull-right">
                                <a href="construction.php" class="btn red-btn">Learn more</a>
                            </div>-->
                            <div class="clearfix"></div>
                        </div>

                        <div style="height:100px;"></div>
                    </div>

                    <div class="item">
                        <div class="box">
                            <img src="img/road-work-ahead.jpg" alt="">
                        </div>
                        <div class="carousel-caption carousel-caption-below-image">
                            <div class="w1 pull-left">
                                <a href="construction.php">
                                    <span class="title">STAY INFORMED ABOUT CONSTRUCTION</span>
                                </a>
                            </div>
                            <!--<div class="w2 pull-right">
                                <a href="construction.php" class="btn red-btn">Learn more</a>
                            </div>-->
                            <div class="clearfix"></div>
                        </div>

                        <div style="height:100px;"></div>
                    </div>


                </div>
                <!-- Carousel nav -->
                <div class="controlContainer" style="width:100%;margin-left:0px;margin-right:0px">
                    <a class="carousel-control left" href="#quartumCarousel" data-slide="prev"></a>
                    <a class="carousel-control right" href="#quartumCarousel" data-slide="next"></a>
                </div>
            </div>
        </div>
        <!-- END SLIDER -->
    </div>
</div>



<div id="servicesText">

    <div id="titleRowsmall">
        <div class="container singlePostnomargin">
            <h1>ABOUT THE PROJECT</h1>
        </div>
    </div>

    <div class="container singlePostnomargin">
        <div class="row">
            <div class="span12">
                <p class="pop-more">
                    The Oregon Department of Transportation (ODOT) has completed the design phase for SE Powell Boulevard from 
                    SE 122nd to SE 136th Avenues. Construction activities began in November 2018 and will last through 2020. 
                    The project will build much-needed roadway, bike and pedestrian safety improvements including: 
                </p>
                    <table style="width:100%;">
                        <tr>
                        <td style="width:5%;vertical-align:top"><p style="font-weight:bold;"></p></td>
                        <td style="width:95%;vertical-align:top">
                            <ul style="padding-left: 20px;">
                                <li>Sidewalks where there are none now</li>
                                <li>Better crosswalks so people can cross the road safely</li>
                                <li>New curb separated bicycle lanes </li>
                                <li>Center turn lanes for cars, buses and trucks for safer turns and to reduce back-ups </li>
                                <li>Storm drains to prevent water from pooling on the road</li>
                            </ul>
                        </td>
                        </tr>
                    </table>

                <br/>
                <a href="construction.php">Learn more about potential construction impacts.</a>
                <br/>    
                <br/>    


            </div>

            <div class="clearfix"></div>
        </div>
    </div>

    <br/><br/>

    <div id="titleRowsmall">
        <div class="container singlePostnomargin">
            <h1>
                Anticipated Construction Timeline
            </h1>
        </div>
    </div>
    <div class="container singlePostnomargin">
        <p><img src="./img/Outer_Powell_Timeline4web_03052019.png" alt=""></p>
    </div>
    <br/>
    <div class="container singlePostnomargin">
        <div class="row">
            <div class="span12">
                <p class="pop-more">
                    Learn more about
                    construction staging and traffic impacts.
                    &nbsp;&nbsp;&nbsp;<a href="construction.php" class="btn red-btn">Learn more</a>
                </p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>


    <br/>
    <br/>
    <br/>
 



    <div id="titleRowsmall">
        <div class="container singlePostnomargin">
            <h1>
                Construction Impacts
            </h1>
        </div>
    </div>
    <div class="container singlePostnomargin">
        <div class="row">
            <div class="span12">
                <p class="pop-more" style="text-align: center">
                    Don’t be caught off-guard by construction in your neighborhood. Stay informed about what’s
                    happening in your area by clicking on the icons below.
                </p>
                <br />
            </div>

            <div class="clearfix"></div>
        </div>
    </div>


    <div class="container singlePostnomargin">


        <div class="construction-icon-container">

            <a href="construction.php#traffic">
                <div class="construction-icon-wrapper">
                    <p>Traffic</p><img class="construction-icons" src="./img/construction-icons/traffic.png" />
                </div>
            </a>
            <a href="construction.php#bike-ped">
                <div class="construction-icon-wrapper">
                    <p>Bike & Pedestrian </p><img class="construction-icons" src="./img/construction-icons/bike-ped.png" />
                </div>
            </a>
            <a href="construction.php#driveway">
                <div class="construction-icon-wrapper">
                    <p>Driveway Access</p><img class="construction-icons" src="./img/construction-icons/driveway.png" />
                </div>
            </a>
            <a href="construction.php#noise">
                <div class="construction-icon-wrapper">
                    <p>Noise</p><img class="construction-icons" src="./img/construction-icons/noise.png" />
                </div>
            </a>
            <a href="construction.php#tree-removal">
                <div class="construction-icon-wrapper">
                    <p>Tree Removal & Utility Relocations</p><img class="construction-icons" src="./img/construction-icons/tree-removal.png" />
                </div>
            </a>
            <a href="construction.php#bus">
                <div class="construction-icon-wrapper">
                    <p>Bus</p><img class="construction-icons" src="./img/construction-icons/bus.png" />
                </div>
            </a>
        </div>

        <br />
        <br />

        <div class="row">
            <h3 class="sign-up-button">
                <a href="contact.php" class="btn red-btn red-btn-big">Sign up for construction updates!</a>
            </h3>
            <br />
        </div>

                <table style="width:100%;">
                    <tr>
                        <td style="width:13%;vertical-align:top"><p style="font-weight:bold;">Translation/Interpretation Accommodations</p></td>
                        <td style="width:87%;vertical-align:top">
                            <ul >
                                <li>For ADA (Americans with Disabilities Act) or Civil Rights Title VI accommodations, translation/interpretation services, or more information call 503-731-4128, TTY 800-735-2900 or Oregon Relay Service 7-1-1.</li>
                                <li>Si desea obtener información sobre este proyecto traducida al español, sírvase llamar al 503-731-4128.</li>
                                <li>Если вы хотите, чтобы информация об этом проекте была переведена на русский язык, пожалуйста, звоните по телефону 503-731-4128.</li>
                                <li>如果您想瞭解這個項目翻譯成 繁體中文 的相關資訊，請致電（503）731-4128. 如果您想了解这个项目翻译成 简体中文 的相关信息，请致电503-731-4128.</li>
                                <li>이 프로젝트에 관한 한국어로 된 자료 신청방법 전화: 503-731-4128.</li>
                                <li>Nếu quý vị muốn thông tin về dự án này được dịch sang tiếng Việt, xin gọi 503-731-4128.</li>
                            </ul>
                        </td>
                    </tr>
                </table>

    </div>

    <br />
    <br />


    <div class="container singlePostnomargin">
        <div class="singlePost singlePostnomargin">
            <blockquote>
                <p>
                    Working towards a safer Outer Powell for everyone.
                </p>
            </blockquote>
        </div>
    </div>
    <div class="container"></div>



    <div id="whatWeDoRow" class="green-row">
        <div class="container">

            <h2>
                <span class="lft"></span>
                <span class="mid">Stay Connected</span>
                <span class="rt"></span>
            </h2>

            <div class="row">
                <div class="span4">
                    <div class="row">
                        <div class="span1">
                            <img src="http://outerpowellsafety.org/img/content/iconDesign.png" alt="Design" />
                        </div>
                        <div class="span3">
                            <div class="moveMe">
                                <h3>Participate</h3>
                                <p>Attend a public meeting near you, or join our online meeting. You can learn more
                                    about the
                                    project, talk to our project team, and give us your input.</p>
                                <a href="get-involved.php" class="btn btn-green" href="#">attend a meeting</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="row">
                        <div class="span1">
                            <img src="http://outerpowellsafety.org/img/content/iconMap.png" alt="Development" />
                        </div>
                        <div class="span3">
                            <div class="moveMe">
                                <h3>Comment</h3>
                                <p>Tell us what you think! Use our interactive map to show us exactly what and
                                    where
                                    your project
                                    concerns are. Put as many flags on the map as you’d like. We’ll read them all.</p>
                                <a href="map.php" class="btn btn-green" href="#">map your comment</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="row">
                        <div class="span1">
                            <img src="http://outerpowellsafety.org/img/content/iconPromotion.png" alt="Promotion" />
                        </div>
                        <div class="span3">
                            <div class="moveMe">
                                <h3>Subscribe</h3>
                                <p>Sign up for the project mailing list to receive occasional updates and notices
                                    about
                                    public
                                    meetings and events.</p>
                                <a href="contact.php" class="btn btn-green" href="#">join the list</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="recentWorksRow">
        <div class="container">

            <h2>
                <span class="lft"></span>
                <span class="mid">past news</span>
                <span class="rt"></span>
            </h2>

            <ul class="thumbnails withHover">
                <li class="span3">
                    <a href="http://koin.com/2015/07/06/funds-earmarked-to-upgrade-outer-se-powell-blvd/" class="thumbnail corner"
                        target="_blank">
                        <span class="topCorner"></span>
                        <img src="http://outerpowellsafety.org/img/content/news-item-funds.jpg" alt="">
                        <div class="contenthover"></div>
                        <span class="bottomCorner"></span>
                    </a>
                    <div class="caption">
                        <h3>
                            <a href="http://portlandtribune.com/pt/9-news/156358-powell-work-aims-to-improve-street-safety"
                                target="_blank">Funds earmarked to upgrade Outer SE Powell Blvd</a>
                        </h3>
                        <p>A total of $20 million may be coming to improve one of Portland’s dangerous stretches of
                            road – Outer
                            Southeast Powell Boulevard.</p>
                    </div>
                </li>
                <li class="span3">
                    <a href="http://portlandtribune.com/pt/9-news/156358-powell-work-aims-to-improve-street-safety"
                        class="thumbnail corner" target="_blank">
                        <span class="topCorner"></span>
                        <img src="http://outerpowellsafety.org/img/content/home-recent-works-2.jpg" alt="">
                        <div class="contenthover"></div>
                        <span class="bottomCorner"></span>
                    </a>
                    <div class="caption">
                        <h3>
                            <a href="http://portlandtribune.com/pt/9-news/156358-powell-work-aims-to-improve-street-safety"
                                target="_blank">Improving Safety</a>
                        </h3>
                        <p>Walking, biking, driving and navigating a wheelchair on Powell Boulevard through East
                            Portland should
                            be safer this fall — courtesy of a $5.5 million state project launched this week.</p>
                    </div>
                </li>
                <li class="span3">
                    <a href="http://www.oregonlive.com/opinion/index.ssf/2014/01/portland_needs_more_money_for.html"
                        class="thumbnail corner" target="_blank">
                        <span class="topCorner"></span>
                        <img src="http://outerpowellsafety.org/img/content/home-recent-works-3.jpg" alt="">
                        <div class="contenthover"></div>
                        <span class="bottomCorner"></span>
                    </a>
                    <div class="caption">
                        <h3>
                            <a href="http://www.oregonlive.com/opinion/index.ssf/2014/01/portland_needs_more_money_for.html"
                                target="_blank">Support for pedestrian safety</a>
                        </h3>
                        <p>Read this guest opinion from the Oregonian about the need for investing in pedestrian
                            safety
                            improvements.</p>
                    </div>
                </li>
                <li class="span3">
                    <a href="http://www.opb.org/news/series/east-of-82nd-a-closer-look-at-east-portland/east-of-82nd-city-struggles-to-make-neighborhoods-walkable/"
                        class="thumbnail corner" target="_blank">
                        <span class="topCorner"></span>
                        <img src="http://outerpowellsafety.org/img/content/home-recent-works-4.jpg" alt="">
                        <div class="contenthover"></div>
                        <span class="bottomCorner"></span>
                    </a>
                    <div class="caption">
                        <h3>
                            <a href="http://www.opb.org/news/series/east-of-82nd-a-closer-look-at-east-portland/east-of-82nd-city-struggles-to-make-neighborhoods-walkable/"
                                target="_blank">Creating walkable neighborhoods</a>
                        </h3>
                        <p>OPB captures community sentiment about the relationship between transportation safety
                            and
                            walkable
                            neighborhoods.
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <!-- <div id="aboutUsRow" class="green-row" >
		<div class="container">
			<h2>
				<span class="lft"></span><span class="mid">About us</span><span class="rt"></span>
			</h2>
			<div class="row">
				<div class="span8">
					<p>Nunc diam eros, vestibulum eget commodo non, egestas et urna. Aenean ipsum purus, varius quis malesuada sit amet, eleifend ac est. Sed adipiscing dictum neque, vitae euismod ante dictum. In hac habitasse platea dictumst. Phasellus non sapien nec risus convallis venenatis. Morbi erat urna, malesuada vitae mollis sed, facilisis at felis. Fusce aliquet facilisis orci, ut gravida ligula viverra quis. Aliquam sed accumsan risus. Duis at pretium arcu.</p>
					<p>Nulla nisl est, gravida vel iaculis vel, porttitor a eros. Sed laoreet sagittis nisi ut. Sed aliquet rutrum mi a molestie. Proin convallis posuere aliquet.  Maecenas non lacus sit amet diam fermentum pellentesque nec non ligula.</p>

				</div>
				<div class="span4">
					<div class="smallSlider">
						<div id="myCarousel" class="carousel slide">
						  <!-- Carousel items -->
    <!--  <div class="carousel-inner">
						    <div class="active item"><div class="aut1">James J. Douglas <span>Nonexistent Inc.</span></div><div class="box"><span class="tip"></span><p>Morbi non augue neque, luctus blandit nisi. Integer imperdiet mauris non turpis molestie tinc dunt. Ut aliquet cursus risus vestibulum elit nibh vulputate purus.</p></div></div>
						    <div class="item"><div class="aut1">John Howard <span>Nonexistent</span></div><div class="box"><span class="tip"></span><p>Morbi non augue neque, luctus blandit n]uet cursus risus vestibulum elit nibh vulputate purus.</p></div></div>
						    <div class="item"><div class="aut1">Michael Doe <span>ABC Inc.</span></div><div class="box"><span class="tip"></span><p>Morbi non augue neque, luctus blandit nisi. Integer  elit nibh vulputate purus. . Integer imperdiet mauris non turpis molest Ut aliquet cursus risus ve</p></div></div>
						  </div>
						  <!-- Carousel nav -->
    <!--   <a class="carousel-control left" href="#myCarousel" data-slide="prev"></a>
						  <a class="carousel-control right" href="#myCarousel" data-slide="next"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>  -->

    <!-- 	<div id="stayTunedRow">
		<div class="container">
			<h2>
				<span class="lft"></span><span class="mid">Stay tuned</span><span class="rt"></span>
			</h2>

			<div class="row">
				<div class="span6 postShowcase">
					<div class="row">
						<div class="span2">
						    <ul class="thumbnails">
							    <li class="span2">
							    	<a href="#" class="thumbnail corner">
									    <span class="topCorner"></span>
                      <img src="img/content/home-stay-tuned-blog.jpg" alt="">
									    <span class="bottomCorner"></span>
								    </a>
							    </li>
							</ul>
						</div>
						<div class="span4">
							<h3><a href="#">Post number one</a></h3>
							<p>Morbi erat urna, malesuada vitae mollis sed, facilisis at felis. Fusce aliquet facilisis orci, ut gravida ligula viverra quis. Aliquam sed accum risus. Duis at pretium arcu. Etiam lectus nisl, rhoncus quis ultricies eu, pellentesque non massa. Duis tempus placerat lectus, sit amet lacinia justo ultrices. Curabitur sit amet nunc. Sed ornare elit lorem. Integer mattis purus ut eros vulputate nec posuere nisl ultrices.</p>
							<p class="summary"><span class="datetime">3 August, 2012</span> <span class="dividers">||</span> <a href="#">3 comments</a></p>
						</div>
					</div>
				</div>
				<div class="span6 newsData">
					<div class="row">
						<div class="span3">
              <div class="tweets"></div>

							<a href="#" class="btn twitterBtn">Follow us</a>
						</div>
						<div class="span3">
							 <ul class="thumbnails">
							    <li>
                    <a href="#" class="thumbnail"><img src="img/content/home-stay-tuned-instagram-1.jpg" alt=""></a>
							    </li>
							    <li>
                    <a href="#" class="thumbnail"><img src="img/content/home-stay-tuned-instagram-2.jpg" alt=""></a>
							    </li>
							    <li>
                    <a href="#" class="thumbnail"><img src="img/content/home-stay-tuned-instagram-3.jpg" alt=""></a>
							    </li>
							    <li>
                    <a href="#" class="thumbnail"><img src="img/content/home-stay-tuned-instagram-4.jpg" alt=""></a>
							    </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->

    <?php include 'footer.php'; ?>