<?php include 'header.php'; ?>

 <div class="container">
  <br />
      <a href="img/construction-timeline-full.png" class="colorUp">
    <img src="img/construction-timeline-full.png" alt=" ">
</a>


</div>

<!--
<div id="Schedule information">
    <div class="container">
        <h2>
            <span class="lft"></span><span class="mid">Project History</span><span class="rt"></span>
        </h2>

        <div class="row">
            <div class="span8">
                <p>The project follows the 2010 - 2012 Outer Powell Boulevard Conceptual Design Plan, a City of Portland and ODOT project. This study identified the need for corridor improvements, including sidewalks, curb ramps, pedestrian crossings, stormwater facilities and better, safer connections to transit, housing, and shopping. It also proposed adding a new center lane to provide a safe area for vehicles to turn through the corridor.</p><p> As a first step, ODOT began implementing some elements of the concept plan in 2013. This work included paving, striping, and adding four new signalized pedestrian crossings to help improve corridor safety immediately while the larger planning effort begins..</p>

            </div>
            <div class="span4">
                <div class="smallSlider">
                    <div id="myCarousel" class="carousel slide">
                        <! - -  Carousel items - - >
                        <div class="carousel-inner">
                            <div class="active item">
                                <div class="aut1 lightbackground">2010 - 2013<br /><a href="http://www.portlandoregon.gov/transportation/53084" target="_blank">Conceptual Design Plan</a></div>
                                <div class="box"><span class="tip"></span>

                                    <p>The City of Portland Bureau of Transportation, in coordination with the ODOT, developed a conceptual design plan for Outer SE Powell Blvd. from the I-205 to SE 174th Ave.</p></div>
                            </div>
                            <div class="item">
                                <div class="aut1 lightbackground">2013 - 2014<br />Interim Safety Improvements</div>
                                <div class="box"><span class="tip"></span>

                                    <p>As a first step, ODOT began implementing some elements of the concept plan in 2013. This work included paving, striping, and adding four new signalized pedestrian crossings</p></div>
                            </div>
                           
                        </div>
                        <! - - Carousel nav - - >
                        <a class="carousel-control left" href="#myCarousel" data-slide="prev"></a>
                        <a class="carousel-control right" href="#myCarousel" data-slide="next"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->


<?php include 'footer.php'; ?>