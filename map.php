<?php include 'header.php'; ?>

<div id="titleRow">
    <div class="container nobottommargin">
        <h1 class="work">
            <span class="lft"></span><span class="mid">Comment Map</span><span class="rt"></span>
        </h1>
    </div>
</div>

<div class="singleProject">

    <div class="container">
	<div class=" mobilehideme">
	<p>Add a comment to the map below to submit an idea, question, or concern about a specific location. </p><br/>
	            <div class="acc1">
            <div class="accordion" id="accordion2">
			                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                            How to add a comment
                        </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse">
                        <div class="accordion-inner">
                           
				<ol>
				<li>Click the green "Add a Comment" button at the bottom of the map</li>
				<li>A blue map marker will appear on the map - drag the blue map marker to the location of your comment</li>
				<li>Once you've moved the blue marker to the location you want, click on it to add your comment</li>
				</ol>
                        </div>
                    </div>
	
	</div>
                    </div>
					   </div>
					 
					  
	
       
	
           <iframe src="http://outerpowell.hdrinnovation.com/" width="100%" height="600" frameborder="0" scrolling="auto">
</iframe>
  </div>
      
		 </div>
		
		  <div class="container">
		 <div class="row mobileshowme">
		 <p><a href="http://outerpowell.hdrinnovation.com/" target="_blank">Add a comment to our comment map</a></p>
	<a href="http://outerpowell.hdrinnovation.com/" target="_blank"><img src="img/content/commentmapthmb.jpg"></a>
           

        </div>
		</div>
		
    </div>



<?php include 'footernocomment.php'; ?>