<?php include 'header.php'; ?>

<div id="titleRow">
    <div class="container">
        <h1>
            <span class="lft"></span>
            <span class="mid">Resources</span>
            <span class="rt"></span>
        </h1>
    </div>
</div>

<div class="singleProject">
    <div class="container">
        <div class="row">

            <div class="span8">
                <div class="comments">
                    <h5>
                        <span class="lft"></span>
                        <span class="mid">Frequently Asked Questions</span>
                        <span class="rt"></span>
                    </h5>
                </div>
                <div class="acc1">
                    <div class="accordion" id="accordion2">

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle active" data-toggle="collapse" data-parent="#accordion1"
                                    href="#collapseOne">
                                    HOW WILL THIS PROJECT AFFECT ME?
                                </a>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <p>
                                        When it’s finished, this project will help you get around outer SE Powell more
                                        safely. But construction will bring temporary inconveniences like traffic
                                        delays, noise and detours. In some cases, changes will need to be made to
                                        driveways or the area where
                                        property meets the road.
                                    </p>
                                    <p>
                                        If you live, work or own property on Powell Boulevard between SE 122nd and SE
                                        136th Avenues, your driveway or frontage may need changes. If changes are
                                        needed, an ODOT team member will contact you.
                                    </p>
                                    <p>
                                        The project is in the early stages of design and the extent of impacts during
                                        construction is still unknown. The project team will continue to update this
                                        website and communicate with the public as more information becomes available.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                    WHEN MIGHT WE SEE CONSTRUCTION HAPPEN?
                                </a>
                            </div>
                            <div id="collapseTwo" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        Construction on Powell Boulevard improvements from SE 122nd to SE 136th Avenues
                                        is anticipated to start in early 2019 and last about two years.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    HOW DID WE GET HERE?
                                </a>
                            </div>
                            <div id="collapseThree" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        From 2014 to 2016, ODOT conducted planning, environmental, and initial design
                                        work for the four-mile stretch of Powell Boulevard east of I-205 from
                                        approximately SE 99th Avenue to the Portland/Gresham city limits at SE 176th
                                        Avenue. In fall 2015, the state Legislature approved $17 million to design and
                                        construct the SE 122nd to SE 136th Avenue section of Powell (which is why this
                                        project is underway!).
                                    </p>
                                    <p>
                                        Prior to the planning and environmental phase, a preliminary study was
                                        performed as a joint effort between ODOT and the Portland Bureau of
                                        Transportation. From this study, the <a href="http://www.portlandoregon.gov/transportation/53084"
                                            target="_blank">2010-2013 Outer Powell Boulevard Conceptual Design Plan</a>
                                        was developed.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                    WHY IS THIS PROJECT NEEDED?
                                </a>
                            </div>
                            <div id="collapseFour" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        Fatal and serious injury collisions affect the lives of many Oregonians every
                                        year. In fact, since 2003 the intersection at Powell Boulevard and SE 122nd
                                        Avenue has been one of the State’s top 5 percent sites for the number and
                                        severity of crashes. This
                                        is why the section of Powell Boulevard from SE 122nd Avenue and SE 136th Avenue
                                        has been prioritized for roadway safety improvements. Sidewalks, dedicated bike
                                        facilities, cross walks and center turn lanes have
                                        been identified to help significantly reduce the number and severity of
                                        collisions in this area.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                                    WHO IS INVOLVED?
                                </a>
                            </div>
                            <div id="collapseFive" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        ODOT is leading the Outer Powell Transportation Safety Project with support
                                        from a consultant team led by HDR Engineering. A Community Advisory Group and
                                        other technical and policy committees met throughout the planning and
                                        environmental phase to guide
                                        the project. The Community Advisory Group will continue to meet and serve as
                                        community liaisons. Also, public meetings and community activities will
                                        continue to be held. Updates will be posted to this website as
                                        the project progresses. Stay tuned for project updates and ways to be involved.
                                    </p>
                                </div>
                            </div>
                        </div>


                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
                                    What improvements were made in 2013?
                                </a>
                            </div>
                            <div id="collapseSix" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>The 2013 Outer Powell Safety Improvements Project was an $8 million project to
                                        help improve safety on the corridor immediately. These improvements have been
                                        completed:</p>
                                    <ul>
                                        <li>Widening the road’s shoulders for pedestrians and bicyclists.</li>
                                        <li>Adding a raised bumpy strip along the edge of the vehicle lanes — known as
                                            rumble strips — to warn drivers if they cross into the shoulder.</li>
                                        <li>Adding 64 curb cuts at intersections to provide better access for
                                            pedestrians, wheelchairs and strollers.
                                        </li>
                                        <li>Installing new pedestrian crosswalks with rapid-flash beacons at Southeast
                                            119th, 141st and 156th Avenue intersections with Powell.</li>
                                        <li>Adding new sensors at all eight main intersections on Outer Powell that
                                            detect when someone is running a red light, which triggers a delay in green
                                            lights for the cross traffic and helps avoid T-boning collisions.</li>
                                        <li>Adding sensors at those eight intersections that make it easier for police
                                            to know when people run a red light, and flag them down to issue tickets.
                                        <li>Adding four signs that flash motorists’ speed, two in each direction.</li>
                                    </ul>
                                    <p>ODOT also invested significant funding for a Transportation Safety Initiative
                                        along Outer Powell: “Slow Down, Don’t Go Around” consisting of billboard signs,
                                        window clings and lawn signs. Funding was provided by ODOT
                                        to collaborate with the Portland Police Bureau, Multnomah County Sheriff’s
                                        Office and Oregon State Police to provide safety and work zone enforcement
                                        before, during and after the project was complete.</p>

                                </div>
                            </div>
                        </div>


                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
                                    What improvements were made in 2014?
                                </a>
                            </div>
                            <div id="collapseSeven" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <ul>
                                        <li>ODOT installed two traffic separators at SE 122nd and Powell and re-signed
                                            and configured parking on the south side of SE Powell from SE 123rd to SE
                                            125th to prevent parked cars from blocking the areas designated
                                            for bicyclists and pedestrians.</li>
                                        <li>A new pedestrian crosswalk with rapid-flash beacon was installed at the SE
                                            168th Avenue intersection with Powell.</li>
                                    </ul>

                                </div>
                            </div>
                        </div>


                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseEight">
                                    WHAT OUTREACH IS BEING DONE?
                                </a>
                            </div>
                            <div id="collapseEight" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        Making sure the public gets the information they need and has the opportunity
                                        to provide input and ask questions is an important aspect of this project.
                                        Continued outreach through public open houses, email and post mail
                                        notifications, targeted community activities and information through this
                                        website will be provided throughout the project.
                                    </p>
                                    <p>
                                        Activities so far have included:
                                    </p>
                                    <ul>
                                        <li>Fifteen interviews with individuals and organizations that work with people
                                            who speak Russian, Vietnamese, Chinese, Spanish and other languages to help
                                            inform and guide our public engagement with these populations to ensure we
                                            are involving everyone in this project.</li>
                                        <li>Seven focus groups with faith-based and social service organizations
                                            working along and near Outer Powell.</li>
                                        <li>Individual Outer Powell Community Walks spoken in Chinese, Russian, and
                                            Spanish, Vietnamese, and English.</li>
                                        <li>An Outer Powell Safety Project Community Advisory Group, representing
                                            diverse organizations and interests has been established. There have been
                                            seven group meetings.</li>
                                        <li>Hosted three open house events.</li>
                                        <li>Hosted a community bike ride.</li>
                                        <li>Had an information booth at community events including ‘Powellhurst-Gilbert
                                            National Night Out’ and ‘Festival of Nations’ in outer east Portland.</li>
                                        <li>Visited over 60 businesses between SE 99th Avenue and SE 174th providing
                                            information about the project.</li>
                                        <li>Conducted interviews with 30 TriMet Route #9 bus users along Powell
                                            Boulevard.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <!--
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle active" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                    Why should I get involved now?
                                </a>
                            </div>
                            <div id="collapseTwo" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <p>If you want to have the greatest say in what Outer Powell Boulevard looks like in 20 years, now is the best time to be involved. The Outer Powell Transportation Safety Project will be comparing options and trade-offs
                                        to make the road safer for everyone. There will be many decisions to make, such as how wide sidewalks and bike lanes should be, where turn lanes and medians should go, whether certain trees should be removed or
                                        lighting added for better visibility at night, where bus stops should be placed, etc.</p>
                                    <p>It’s a long list! Your voice matters now because in later phases of the project, it is often too late to comment on these details – they have already been decided. By getting involved in the Outer Powell Transportation
                                        Safety Project now, you will have the best opportunity to influence the project in the future.</p>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                    How is this project different from the prior Conceptual Design Plan?
                                </a>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>The
                                        <a href="http://www.portlandoregon.gov/transportation/53084" target="_blank">2010-2013 Outer Powell Boulevard Conceptual Design Plan</a>, led by the City of Portland and ODOT, looked at the expected needs of drivers,
                                        people who walk, ride their bike, or take the bus along Outer Powell Boulevard during the next 20 years.</p>
                                    <p>One of the plan’s recommendations was to widen the road to create three traffic lanes (one in each direction plus a center turn lane) for much of the corridor from I-205 to SE 174th Ave. Other suggested improvements
                                        included adding sidewalks, curb ramps, pedestrian crossings, storm water facilities, and better, safer connections to transit, housing, and shopping. The plan was adopted by the Portland City Council in June 2012,
                                        with the final report complete in February 2013.</p>
                                    <p>The current Outer Powell Transportation Safety Project, being led by ODOT, picks up where the prior study left off. It will refine the previous road design and study in much greater detail the locations of improvements
                                        and potential impacts on corridor users, property owners and the community. Public involvement will play a big role in this process.</p>
                                    <p>The work currently under way will help ODOT and it partners find future funding for final design and eventually, construction.</p>

                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    How will project decisions be made?
                                </a>
                            </div>
                            <div id="collapseThree" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>Several committees will be making decisions for the Outer Powell Transportation Safety Project. A Community Advisory Group, representing people who live, work and own property along the corridor, is an important part
                                        of project decision making and represents the face of the community in project planning. Members of the Community Advisory Group reflect the diverse population of outer East Portland. The committee will meet every
                                        other month to discuss the project and make recommendations to the Decision Committee.</p>
                                    <p>The Decision Committee has representatives of ODOT, Federal Highway Administration, City of Portland, Metro, TriMet and the Oregon Legislature. This committee also includes the Chair of the Community Advisory Group
                                        as a voting member. The Decision Committee will consider the recommendations of the Community Advisory Group in the selection of a preferred alternative and an agreement on next steps for the project.</p>
                                    <p>Providing input to the Community Advisory Group and Decision Committee will be the HDR consultant team and ODOT staff as well as an Agency Working Group of ODOT agency partners.</p>

                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                    How much will this cost?
                                </a>
                            </div>
                            <div id="collapseFour" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>ODOT is investing approximately $3 million in the Outer Powell Transportation Safety Project. This funding will complete the planning, environmental study and some design work for the corridor. This work will be finished
                                        in late 2016 and will lay the ground work for additional project funding. Next steps will include a final design phase and eventually, construction.</p>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                                    What improvements were made in 2013?
                                </a>
                            </div>
                            <div id="collapseFive" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>The 2013 Outer Powell Safety Improvements Project was an $8 million project to help improve safety on the corridor immediately. These improvements have been completed:</p>
                                    <ul>
                                        <li>Widening the road’s shoulders for pedestrians and bicyclists.</li>
                                        <li>Adding a raised bumpy strip along the edge of the vehicle lanes — known as rumble strips — to warn drivers if they cross into the shoulder.</li>
                                        <li>Adding 64 curb cuts at intersections to provide better access for pedestrians, wheelchairs and strollers.
                                        </li>
                                        <li>Installing new pedestrian crosswalks with rapid-flash beacons at Southeast 119th, 141st and 156th Avenue intersections with Powell.</li>
                                        <li>Adding new sensors at all eight main intersections on Outer Powell that detect when someone is running a red light, which triggers a delay in green lights for the cross traffic and helps avoid T-boning collisions.</li>
                                        <li>Adding sensors at those eight intersections that make it easier for police to know when people run a red light, and flag them down to issue tickets.
                                            <li>Adding four signs that flash motorists’ speed, two in each direction.</li>
                                    </ul>
                                    <p>ODOT also invested significant funding for a Transportation Safety Initiative along Outer Powell: “Slow Down, Don’t Go Around” consisting of billboard signs, window clings and lawn signs. Funding was provided by ODOT
                                        to collaborate with the Portland Police Bureau, Multnomah County Sheriff’s Office and Oregon State Police to provide safety and work zone enforcement before, during and after the project was complete.</p>

                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
                                    What improvements were made in 2014?
                                </a>
                            </div>
                            <div id="collapseSix" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <ul>
                                        <li>ODOT installed two traffic separators at SE 122nd and Powell and re-signed and configured parking on the south side of SE Powell from SE 123rd to SE 125th to prevent parked cars from blocking the areas designated
                                            for bicyclists and pedestrians.</li>
                                        <li>A new pedestrian crosswalk with rapid-flash beacon was installed at the SE 168th Avenue intersection with Powell.</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
                                    What outreach is being done as part of this project?
                                </a>
                            </div>
                            <div id="collapseSeven" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>A lot of community outreach will happen during the Outer Powell Transportation Safety Project. ODOT is committed to ensuring that people who don’t typically engage in public planning processes have a voice. Activities
                                        so far have included:</p>
                                    <ul>
                                        <li>Fifteen interviews with individuals and organizations that work with people who speak Russian, Vietnamese, Chinese, Spanish and other languages to help inform and guide our public engagement with these populations
                                            to ensure we are involving everyone in this project.</li>
                                        <li>Seven focus groups with faith-based and social service organizations working along and near Outer Powell.</li>
                                        <li>Individual Outer Powell Community Walks spoken in Chinese, Russian, and Spanish, Vietnamese, and English.</li>
                                        <li>An Outer Powell Safety Project Community Advisory Group, representing diverse organizations and interests, has been established.</li>
                                        <li>Hosted two open house events.</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseEight">
                                    When might we see construction happening?
                                </a>
                            </div>
                            <div id="collapseEight" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>
                                        The current planning effort for the corridor will be complete in 2016. The state Legislature has provided $17 million to design and construct the SE 122nd Avenue to SE 136th Avenue section of Powell. Final design work is expected to begin in spring 2016.
                                        Construction is expected in 2019. Additional design and construction phases beyond that depend on additional funding for the project.
                                    </p>
                                </div>
                            </div>
                        </div>

-->

                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="acc1">
                    <div class="accordion" id="accordion3">

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseTen">
                                    Project Documents
                                </a>
                            </div>
                            <div id="collapseTen" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <ul class="documents">
                                        <li>
                                            <a href="doc/infographic.pdf" target="_blank">Infographic</a>
                                        </li>
                                        <li>
                                            <a href="doc/conceptual-design-plan.pdf" target="_blank">2010-2013
                                                Conceptual Design Plan</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_DRAFT_API_12.4.15_LoRes.pdf" target="_blank">Draft Area
                                                of Potential Impact</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_cross_sections_2015-08-11.pdf" target="_blank">Proposed
                                                Cross Sections</a>
                                        </li>
                                        <li>
                                            <a href="tiger">TIGER Grant Application Materials</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_ES_working_2016-06-06_2nd_version.pdf" target="_blank">Draft
                                                Environmental Summary Report</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_Multimodal_Options_Memo.pdf" target="_blank">Multimodal
                                                Options Memo</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_Boards_03172017.pdf" target="_blank">Open House Display
                                                Boards &mdash; March 2017</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_ProjectMap_03172017.pdf" target="_blank">Project Map
                                                &mdash; March 2017</a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseNine">
                                    Fact Sheets
                                </a>
                            </div>
                            <div id="collapseNine" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <ul class="documents">
                                        <li>
                                            <a href="doc/factsheets/Construction Fact Sheet (English).pdf" target="_blank">Factsheet 2018
                                                (English)</a>
                                        </li>
                                        <li>
                                            <a href="doc/factsheets/Construction Fact Sheet (Russian).pdf" target="_blank">Factsheet 2018
                                                (Russian)</a>
                                        </li>
                                        <li>
                                            <a href="doc/factsheets/Construction Fact Sheet (Vietnamese).pdf" target="_blank">Factsheet 2018
                                                (Vietnamese)</a>
                                        </li>
                                        <li>
                                            <a href="doc/factsheets/Construction Fact Sheet (Spanish).pdf" target="_blank">Factsheet 2018
                                                (Spanish)</a>
                                        </li>
                                        <li>
                                            <a href="doc/factsheets/Construction Fact Sheet (Chinese).pdf" target="_blank">Factsheet 2018
                                                (Chinese)</a>
                                        </li> 
                                        <li>
                                            <a href="doc/factsheets/OPTSP_Factsheet_2017-03-20_ENG.pdf" target="_blank">Factsheet
                                                (English)</a>
                                        </li>
                                        <li>
                                            <a href="doc/factsheets/OPTSP_factsheet_Summer_2016_091616_RU.pdf" target="_blank">Factsheet
                                                (Russian)</a>
                                        </li>
                                        <li>
                                            <a href="doc/factsheets/OPTSP_factsheet_Summer_2016_091616_VI.pdf" target="_blank">Factsheet
                                                (Vietnamese)</a>
                                        </li>
                                        <li>
                                            <a href="doc/factsheets/OPTSP_factsheet_Summer_2016_091616_SP.pdf" target="_blank">Factsheet
                                                (Spanish)</a>
                                        </li>
                                        <li>
                                            <a href="doc/factsheets/OPTSP_factsheet_Summer_2016_091616_CH.pdf" target="_blank">Factsheet
                                                (Chinese)</a>
                                        </li>                                       
                                    </ul>


                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseEleven">
                                    Committee Summaries
                                </a>
                            </div>
                            <div id="collapseEleven" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <ul class="documents">
                                        <li>
                                            <a href="doc/Community_Advisory_Group_Mtg_1_2014_12_03.pdf" target="_blank">Community
                                                Advisory Group Meeting #1</a>
                                        </li>
                                        <li>
                                            <a href="doc/Community_Advisory_Group_Mtg_2_2015_01_26.pdf" target="_blank">Community
                                                Advisory Group Meeting #2</a>
                                        </li>
                                        <li>
                                            <a href="doc/Community_Advisory_Group_Mtg_3_2015_03_18.pdf" target="_blank">Community
                                                Advisory Group Meeting #3</a>
                                        </li>
                                        <li>
                                            <a href="doc/Community_Advisory_Group_Mtg_4_2015_06_08.pdf" target="_blank">Community
                                                Advisory Group Meeting #4</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_CAG_Meeting_5_Presentation.pdf" target="_blank">Community
                                                Advisory Group Meeting #5</a>
                                        </li>
                                        <li>
                                            <a href="doc/MM_OPTSP_CAG_Mtg_6_2015_12_02.pdf" target="_blank">Community
                                                Advisory Group Meeting #6</a>
                                        </li>
                                        <li>
                                            <a href="doc/MM_OPTSP_CAG_Mtg_7_2016_06_15.pdf" target="_blank">Community
                                                Advisory Group Meeting #7</a>
                                        </li>
                                        <li>
                                            <a href="doc/MM_OPTSP_CAG_Mtg_8_2017-03-20.pdf" target="_blank">Community
                                                Advisory Group Meeting #8</a>
                                        </li>
                                        <li>
                                            <a href="doc/Decision_Committee_Mtg_1_2014_12_11.pdf" target="_blank">Decision
                                                Committee Meeting #1</a>
                                        </li>
                                        <li>
                                            <a href="doc/Decision_Committee_Mtg_2_2015_03_31.pdf" target="_blank">Decision
                                                Committee Meeting #2</a>
                                        </li>
                                        <li>
                                            <a href="doc/Decision_Committee_Mtg_3_2015_06_23.pdf" target="_blank">Decision
                                                Committee Meeting #3</a>
                                        </li>
                                        <li>
                                            <a href="doc/MM_OPTSP_Decision_Committee_Mtg_4_2015_12_08.pdf" target="_blank">Decision
                                                Committee Meeting #4</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseTwelve">
                                    Public Outreach Summaries
                                </a>
                            </div>
                            <div id="collapseTwelve" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <ul class="documents">
                                        <li>
                                            <a href="doc/Open_House_Summary_2014_12_09.pdf" target="_blank">Open House
                                                #1 Summary</a>
                                        </li>
                                        <li>
                                            <a href="doc/Open_House_Summary_2015_03_09.pdf" target="_blank">Open House
                                                #2 Summary</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_Open_House_Summary_2015_09_16_Final.pdf" target="_blank">Open
                                                House #3 Summary</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_September_2018_Open House_Summary.pdf" target="_blank">Open
                                                House #4 Summary</a>
                                        </li>                                        
                                        <li>
                                            <a href="doc/OPTSP_Community_Site_Walks_Summary.pdf" target="_blank">Community
                                                Site Walks Summary</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_Focus_Groups_Summary.pdf" target="_blank">Focus Groups
                                                Summary</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_Bus_Canvassing_Summary.pdf" target="_blank">Bus
                                                Canvassing Summary</a>
                                        </li>
                                        <li>
                                            <a href="doc/OPTSP_Bike_Ride_Summary_2015_08_01.pdf" target="_blank">Bike
                                                Ride Summary</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">

                                <a class="accordion-toggle active" data-toggle="collapse" data-parent="#accordion3"
                                    href="#collapse13">
                                    PROJECT VIDEO FROM PLANNING PHASE
                                </a>
                            </div>
                            <div id="collapse13" class="accordion-body collapse in">
                                <div class="accordion-inner" style="padding-left:0px;">
                                    <div id="main-video-section" style="">
                                        <p id="video-translation-title">Video Translation:</p>
                                        <select id="videoLangSelect" onchange="updateCurrentVideo(this)">
                                            <option selected value="english">English</option>
                                            <option value="spanish">Vea aqui el video en español</option>
                                            <option value="chinese">中文</option>
                                            <option value="vietnamese">Xem bằng tiếng việt ở đây</option>
                                            <option value="russian">Посмотрите на русском языке здесь</option>
                                        </select>
                                    </div>

                                    <div class="" style="width:100%;">
                                        <iframe id="user-picked-video-lang" src="//fast.wistia.net/embed/iframe/bn8uxme3g7"
                                            allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed"
                                            name="wistia_embed" allowfullscreen mozallowfullscreen
                                            webkitallowfullscreen oallowfullscreen msallowfullscreen width="90%" height="350"
                                            style="margin-left:30px;float:left;"></iframe>
                                    </div>

                                    <script src="//fast.wistia.net/assets/external/E-v1.js" async></script>
                                    <script type="text/javascript">
                                        var vidUrls = {
                                            english: '//fast.wistia.net/embed/iframe/bn8uxme3g7',
                                            spanish: '//fast.wistia.net/embed/iframe/xu7lz1jdcx',
                                            vietnamese: '//fast.wistia.net/embed/iframe/fmnboze2g5',
                                            russian: '//fast.wistia.net/embed/iframe/bysyd28z3r',
                                            chinese: '//fast.wistia.net/embed/iframe/etqjtdgkdw'
                                        };

                                        function updateCurrentVideo(e) {
                                            console.log(e.value)
                                            $('#user-picked-video-lang').attr("src", vidUrls[e.value]);
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'footer.php'; ?>