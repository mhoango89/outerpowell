<?php include 'header.php'; ?>

<!-- End Main Menu and Logo -->
<div id="sloganRow">
  <div class="container">
    <h1></h1>
  </div>
</div>
<div id="sliderRow">
  <div class="container">

    <div class="slider">
      <div id="quartumCarousel" class="carousel slide">
        <!-- Carousel items -->
        <div class="carousel-inner">
          <div class="active item">
            <div class="carousel-caption">
              <div class="w1 pull-left">
                <a href="doc/infographic.pdf" target="_blank">
                  <span class="title">Project Infographic</span>
                </a>
                <span class="description">Learn why the Outer Powell Project is needed.</span>
              </div>
              <div class="w2 pull-right">
                <a href="doc/infographic.pdf" class="btn red-btn" target="_blank">see the graphic</a>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="box"><img src="img/content/home-usual-slider-1.jpg" alt=""></div>
          </div>

          <div class="item">
            <div class="carousel-caption">
              <div class="w1 pull-left">
                <a href="/map.php">
                  <span class="title">Tell us what you think</span>
                </a>
                <span class="description">Submit a comment on our interactive comment map.</span>
              </div>
              <div class="w2 pull-right">
                <a href="/map.php" class="btn red-btn">submit a comment</a>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="box"><img src="img/content/home-usual-slider-2.jpg" alt=""></div>
          </div>

          <div class="item">
            <div class="carousel-caption">
              <div class="w1 pull-left">
                <a href="/contact.php">
                  <span class="title">Stay in the loop</span>
                </a>
                <span class="description">Get project updates as they happen</span>
              </div>
              <div class="w2 pull-right">
                <a href="/contact.php" class="btn red-btn">join the mailing list</a>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="box"><img src="img/content/home-usual-slider-3.jpg" alt=""></div>
          </div>
        </div>
        <!-- Carousel nav -->
        <div class="controlContainer">
          <a class="carousel-control left" href="#quartumCarousel" data-slide="prev"></a>
          <a class="carousel-control right" href="#quartumCarousel" data-slide="next"></a>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="servicesText">
  <div id="titleRowsmall">
    <div class="container singlePostnomargin">
      <h1>
        We need your ideas for the future of Outer Powell Boulevard
      </h1>
    </div>
  </div>
  <div class="container singlePostnomargin">

    <div class="row">
      <div class="span6">

        <h4>Welcome!</h4>
        <p>
					Thank you for visiting the Outer Powell Transportation Safety Project website. We’re glad you’re here. The Oregon Department of Transportation has been working to make Outer Powell Boulevard safer for everyone. Whether we drive, walk, ride a bike
          or take the bus on Outer Powell, we all want it to be a safe and comfortable experience. The Outer Powell Transportation Safety Project is your opportunity to have a voice in planning the future of four miles of SE Powell Boulevard between I-205 and
          SE 174th Ave.
				</p><br/>
        <h4>We value your input</h4>
        <p>
					We need your input to help answer some questions about the future of this road, such as “Should sidewalks be added for the entire length of Outer Powell Boulevard?” and “Should center turn lanes be added for cars, and if so, where should they go?”
          Adding things like sidewalks and turn lanes means making Outer Powell Boulevard wider, which affects neighborhoods, businesses and the environment along the road. This new project will involve a lot of people (including you) to help answer these and
          other important questions.
				</p><br />
				<h4>More information to come</h4>
				<p>
					The current planning effort for the corridor will be complete in 2016. The state Legislature has provided $17 million to design and construct the SE 122nd Avenue to SE 136th Avenue section of Powell. Final design work is expected to begin in spring 2016. Construction is expected in 2019. Additional design and construction phases beyond that depend on additional funding for the project.
				</p><br/>
      </div>
      <div class="span6">
        <br/>
        <!-- <iframe src="//fast.wistia.net/embed/iframe/vlcxiq6mgo" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="90%" height="350" style="margin-left:30px; margin-bottom: 30px;"></iframe>
				<script src="//fast.wistia.net/assets/external/E-v1.js" async></script> -->

        <div id="main-video-section" style="">
          <p id="video-translation-title">Video Translation:</p>
          <select id="videoLangSelect" onchange="updateCurrentVideo(this)">
            <!-- <option selected disabled>Choose another language for the video</option> -->
            <option selected value="english">English</option>
            <option value="spanish">Vea aqui el video en español</option>
            <option value="chinese">中文</option>
            <option value="vietnamese">Xem bằng tiếng việt ở đây</option>
            <option value="russian">Посмотрите на русском языке здесь</option>
          </select>

        </div>

        <div class="" style="width:100%;">
          <iframe
            id="user-picked-video-lang"
            src="//fast.wistia.net/embed/iframe/bn8uxme3g7"
            allowtransparency="true"
            frameborder="0"
            scrolling="no"
            class="wistia_embed"
            name="wistia_embed"
            allowfullscreen
            mozallowfullscreen
            webkitallowfullscreen
            oallowfullscreen
            msallowfullscreen
            width="90%"
            height="350"
            style="margin-left:30px;float:left;"></iframe>

        </div>

        <script src="//fast.wistia.net/assets/external/E-v1.js" async></script>
        <script type="text/javascript">
          var vidUrls = {
            english: '//fast.wistia.net/embed/iframe/bn8uxme3g7',
            spanish: '//fast.wistia.net/embed/iframe/xu7lz1jdcx',
            vietnamese: '//fast.wistia.net/embed/iframe/fmnboze2g5',
            russian: '//fast.wistia.net/embed/iframe/bysyd28z3r',
            chinese: '//fast.wistia.net/embed/iframe/etqjtdgkdw'
          };
          function updateCurrentVideo(e) {
            console.log(e.value)
            $('#user-picked-video-lang').attr("src", vidUrls[e.value]);
          }
        </script>
      </div>
      <div class="clearfix"></div>
      <div class="container singlePostnomargin">
        <div class="singlePost singlePostnomargin">
          <blockquote>
            <p>We encourage you to look around, add your name to our mailing list, tell us what you think, and come to an upcoming community meeting. Your voice really does matter.</p>

          </blockquote>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="whatWeDoRow" class="green-row">
  <div class="container">

    <h2>
      <span class="lft"></span>
      <span class="mid">Stay Connected</span>
      <span class="rt"></span>
    </h2>

    <div class="row">
      <div class="span6">
        <div class="row">
          <div class="span1">
            <img src="img/content/iconDesign.png" alt="Design"/>
          </div>
          <div class="span3">
            <div class="moveMe">
              <h3>Participate</h3>
              <p>Attend a public meeting near you, or join our online meeting. You can learn more about the project, talk to our project team, and give us your input.</p>
              <a href="/get-involved.php" class="btn btn-green" href="#">attend a meeting</a>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="span4">
        <div class="row">
          <div class="span1">
            <img src="img/content/iconMap.png" alt="Development"/>
          </div>
          <div class="span3">
            <div class="moveMe">
              <h3>Comment</h3>
              <p>Tell us what you think! Use our interactive map to show us exactly what and where your project concerns are. Put as many flags on the map as you’d like. We’ll read them all.</p>
              <a href="/map.php" class="btn btn-green" href="#">map your comment</a>
            </div>
          </div>
        </div>
      </div> -->
      <div class="span6">
        <div class="row">
          <div class="span1">
            <img src="img/content/iconPromotion.png" alt="Promotion"/>
          </div>
          <div class="span3">
            <div class="moveMe">
              <h3>Subscribe</h3>
              <p>Sign up for the project mailing list to receive occasional updates and notices about public meetings and events.</p>
              <a href="/contact.php" class="btn btn-green" href="#">join the list</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="recentWorksRow">
  <div class="container">

    <h2>
      <span class="lft"></span>
      <span class="mid">Recent news</span>
      <span class="rt"></span>
    </h2>

    <ul class="thumbnails withHover">
      <li class="span3">
        <a href="http://koin.com/2015/07/06/funds-earmarked-to-upgrade-outer-se-powell-blvd/" class="thumbnail corner" target="_blank">
          <span class="topCorner"></span>
          <img src="img/content/news-item-funds.jpg" alt="">
          <div class="contenthover"></div>
          <span class="bottomCorner"></span>
        </a>
        <div class="caption">
          <h3>
            <a href="http://portlandtribune.com/pt/9-news/156358-powell-work-aims-to-improve-street-safety" target="_blank">Funds earmarked to upgrade Outer SE Powell Blvd</a>
          </h3>
          <p>A total of $20 million may be coming to improve one of Portland’s dangerous stretches of road – Outer Southeast Powell Boulevard.</p>
        </div>
      </li>
      <li class="span3">
        <a href="http://portlandtribune.com/pt/9-news/156358-powell-work-aims-to-improve-street-safety" class="thumbnail corner" target="_blank">
          <span class="topCorner"></span>
          <img src="img/content/home-recent-works-2.jpg" alt="">
          <div class="contenthover"></div>
          <span class="bottomCorner"></span>
        </a>
        <div class="caption">
          <h3>
            <a href="http://portlandtribune.com/pt/9-news/156358-powell-work-aims-to-improve-street-safety" target="_blank">Improving Safety</a>
          </h3>
          <p>Walking, biking, driving and navigating a wheelchair on Powell Boulevard through East Portland should be safer this fall — courtesy of a $5.5 million state project launched this week.</p>
        </div>
      </li>
      <li class="span3">
        <a href="http://www.oregonlive.com/opinion/index.ssf/2014/01/portland_needs_more_money_for.html" class="thumbnail corner" target="_blank">
          <span class="topCorner"></span>
          <img src="img/content/home-recent-works-3.jpg" alt="">
          <div class="contenthover"></div>
          <span class="bottomCorner"></span>
        </a>
        <div class="caption">
          <h3>
            <a href="http://www.oregonlive.com/opinion/index.ssf/2014/01/portland_needs_more_money_for.html" target="_blank">Support for pedestrian safety</a>
          </h3>
          <p>Read this guest opinion from the Oregonian about the need for investing in pedestrian safety improvements.</p>
        </div>
      </li>
      <li class="span3">
        <a href="http://www.opb.org/news/series/east-of-82nd-a-closer-look-at-east-portland/east-of-82nd-city-struggles-to-make-neighborhoods-walkable/" class="thumbnail corner" target="_blank">
          <span class="topCorner"></span>
          <img src="img/content/home-recent-works-4.jpg" alt="">
          <div class="contenthover"></div>
          <span class="bottomCorner"></span>
        </a>
        <div class="caption">
          <h3>
            <a href="http://www.opb.org/news/series/east-of-82nd-a-closer-look-at-east-portland/east-of-82nd-city-struggles-to-make-neighborhoods-walkable/" target="_blank">Creating walkable neighborhoods</a>
          </h3>
          <p>OPB captures community sentiment about the relationship between transportation safety and walkable neighborhoods.</p>
        </div>
      </li>
    </ul>
  </div>
</div>

<!-- <div id="aboutUsRow" class="green-row" >
		<div class="container">
			<h2>
				<span class="lft"></span><span class="mid">About us</span><span class="rt"></span>
			</h2>
			<div class="row">
				<div class="span8">
					<p>Nunc diam eros, vestibulum eget commodo non, egestas et urna. Aenean ipsum purus, varius quis malesuada sit amet, eleifend ac est. Sed adipiscing dictum neque, vitae euismod ante dictum. In hac habitasse platea dictumst. Phasellus non sapien nec risus convallis venenatis. Morbi erat urna, malesuada vitae mollis sed, facilisis at felis. Fusce aliquet facilisis orci, ut gravida ligula viverra quis. Aliquam sed accumsan risus. Duis at pretium arcu.</p>
					<p>Nulla nisl est, gravida vel iaculis vel, porttitor a eros. Sed laoreet sagittis nisi ut. Sed aliquet rutrum mi a molestie. Proin convallis posuere aliquet.  Maecenas non lacus sit amet diam fermentum pellentesque nec non ligula.</p>

				</div>
				<div class="span4">
					<div class="smallSlider">
						<div id="myCarousel" class="carousel slide">
						  <!-- Carousel items -->
<!--  <div class="carousel-inner">
						    <div class="active item"><div class="aut1">James J. Douglas <span>Nonexistent Inc.</span></div><div class="box"><span class="tip"></span><p>Morbi non augue neque, luctus blandit nisi. Integer imperdiet mauris non turpis molestie tinc dunt. Ut aliquet cursus risus vestibulum elit nibh vulputate purus.</p></div></div>
						    <div class="item"><div class="aut1">John Howard <span>Nonexistent</span></div><div class="box"><span class="tip"></span><p>Morbi non augue neque, luctus blandit n]uet cursus risus vestibulum elit nibh vulputate purus.</p></div></div>
						    <div class="item"><div class="aut1">Michael Doe <span>ABC Inc.</span></div><div class="box"><span class="tip"></span><p>Morbi non augue neque, luctus blandit nisi. Integer  elit nibh vulputate purus. . Integer imperdiet mauris non turpis molest Ut aliquet cursus risus ve</p></div></div>
						  </div>
						  <!-- Carousel nav -->
<!--   <a class="carousel-control left" href="#myCarousel" data-slide="prev"></a>
						  <a class="carousel-control right" href="#myCarousel" data-slide="next"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>  -->

<!-- 	<div id="stayTunedRow">
		<div class="container">
			<h2>
				<span class="lft"></span><span class="mid">Stay tuned</span><span class="rt"></span>
			</h2>

			<div class="row">
				<div class="span6 postShowcase">
					<div class="row">
						<div class="span2">
						    <ul class="thumbnails">
							    <li class="span2">
							    	<a href="#" class="thumbnail corner">
									    <span class="topCorner"></span>
                      <img src="img/content/home-stay-tuned-blog.jpg" alt="">
									    <span class="bottomCorner"></span>
								    </a>
							    </li>
							</ul>
						</div>
						<div class="span4">
							<h3><a href="#">Post number one</a></h3>
							<p>Morbi erat urna, malesuada vitae mollis sed, facilisis at felis. Fusce aliquet facilisis orci, ut gravida ligula viverra quis. Aliquam sed accum risus. Duis at pretium arcu. Etiam lectus nisl, rhoncus quis ultricies eu, pellentesque non massa. Duis tempus placerat lectus, sit amet lacinia justo ultrices. Curabitur sit amet nunc. Sed ornare elit lorem. Integer mattis purus ut eros vulputate nec posuere nisl ultrices.</p>
							<p class="summary"><span class="datetime">3 August, 2012</span> <span class="dividers">||</span> <a href="#">3 comments</a></p>
						</div>
					</div>
				</div>
				<div class="span6 newsData">
					<div class="row">
						<div class="span3">
              <div class="tweets"></div>

							<a href="#" class="btn twitterBtn">Follow us</a>
						</div>
						<div class="span3">
							 <ul class="thumbnails">
							    <li>
                    <a href="#" class="thumbnail"><img src="img/content/home-stay-tuned-instagram-1.jpg" alt=""></a>
							    </li>
							    <li>
                    <a href="#" class="thumbnail"><img src="img/content/home-stay-tuned-instagram-2.jpg" alt=""></a>
							    </li>
							    <li>
                    <a href="#" class="thumbnail"><img src="img/content/home-stay-tuned-instagram-3.jpg" alt=""></a>
							    </li>
							    <li>
                    <a href="#" class="thumbnail"><img src="img/content/home-stay-tuned-instagram-4.jpg" alt=""></a>
							    </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->

<?php include 'footer.php'; ?>
