<?php include 'header.php'; ?>

<div id="titleRow">
    <div class="container">
        <h1>
            <span class="lft"></span>
            <span class="mid">Get involved</span>
            <span class="rt"></span>
        </h1>
    </div>
</div>

    <div class="container singlePost">
        <h5>
            <span class="lft"></span>
            <span class="mid">Join Us For Upcoming Events</span>
            <span class="rt"></span>
        </h5>
    </div>

<div class="container">
    <img src="img/Outer_Powell_GBC_Flyer_Draft1.3.jpg" alt="">
</div>



<div id="ourBlog">
    <div class="container">
        <div class="row">
            <div class="span9">

                <!-- left container -->
                <div class="singlePost">

                    <!--h5>
                        <span class="lft"></span>
                        <span class="mid">Join Us For Upcoming Events</span>
                        <span class="rt"></span>
                    </h5>-->
                    

                    <!-- EVENT SECITON -->
                    
                    <h2 style="color:#C8584C;">Ground-Breaking Ceremony</h2>
                    <br/>
                    <div>
                        <b>When: </b>&nbsp;Saturday, April 6, 2019 from 10 AM to Noon
                    </div>
                    <br/>
                    <div>
                        <b>Where: </b>&nbsp;
                        <a href="https://www.google.com/maps/place/Safeway/@45.4962789,-122.5392748,15z/data=!4m2!3m1!1s0x0:0xdffe51983bfaff31?ved=2ahUKEwiBzuqLz-vgAhWI3YMKHYD4Cl8Q_BIwCnoECAUQCA" 
                            >Safeway – 3527 SE 122nd Ave., Portland, OR 97236</a>
                            &nbsp;(Side parking lot along SE Powell Blvd.)
                    </div>
                    <br/>

                    <div>
                    After several years of planning, environmental study and design, the Outer Powell Transportation Safety Project is now kicking off 
                    construction. Join us for a ground-breaking ceremony to recognize this big milestone in the project and the many people that helped 
                    get us here.
                    <br/><br/>
                    Let’s celebrate! The project team invites you to gather for a morning with elected officials, agency leaders and community members 
                    to reflect on the past and prepare for the future of Outer Powell.
                    <br/><br/>
                    If you have questions, contact  
                    <a href="mailto:Ellen.Sweeney@odot.state.or.us">Ellen Sweeney</a>, ODOT Community Affairs Coordinator or 503.731.8230.
                    </div>
                    
                    <p style="margin-top: 20px; "></p>
                    <i style="color:#585858;">
                        <div style="margin-top: 6px; padding: 0px 70px 0px 50px;">
                            <b>Accessible Event Information:</b>
                            This event/location is accessible to people with disabilities. Sign language, interpreter service, assisted listening 
                            devices, materials in alternate format, such as Braille, large print, electronic formats and audio cassette tape, and 
                            other accommodations can be available upon advance request. Please contact Ellen Sweeney at least 48 hours prior to 
                            the event at 503.731.8230 and/or fax 503.731.3266.
                        </div>
                        <div style="margin-top: 6px;padding: 0px 70px 0px 50px; ">
                            <b>Request an Interpreter</b>: 
                            Para solicitar un intérprete para la reunión pública, llame al 503.731.8230 al menos 48 horas antes de la reunión.
                            Để yêu cầu một thông dịch viên cho buổi họp công khai, xin gọi điện thoại số 503.731.8230 tối thiểu 48 giờ trước buổi họp.
                            Если Вы нуждаетесь в услугах устного переводчика на общественном собрании, пожалуйста, позвоните по тел. 503.731.8230 как минимум за 48 часов до начала собрания.
                            您参加公共会议时若需要口译员，请于会前至少 48 小时拨打 503.731.3490.
                        </div>
                    </i>
                    </p>
                    <br>
                    <br> 





                    <!-- EVENT SECITON -->
                    <!-- <h2 style="color:#C8584C; ">Upcoming Events</h2>-->
                    <!--<p>
                        There are no public events planned at this time. Check back for updates or 
                        <a href="contact.php ">sign up</a> for the email list to be notified.
                    </p> -->
<!--
                    <h2 style="color:#C8584C; ">Public Open House</h2>
                    <p>
                        Join us for a public open house to learn more about the project and what can be expected during
                        construction. Project team members will be available to share information and answer questions.
                    </p>

                    <p style="padding-left:30px;font-style: italic">
                        Thursday, September 27, 2018
                        <br> 5:30 p.m. – 7:30 p.m.
                        <br> Ron Russell Middle School
                    </p>

                    <br/>
                    <hr>
                    <p>
                        <b>Accessible Event Information:</b> This event/location is accessible to people with disabilities. 
                        Sign language, interpreter service, assisted listening devices, materials in alternate format, such as Braille, large print, electronic formats and 
                        audio cassette tape, and other accommodations can be available upon advance request. Please contact Ellen Sweeney as soon as possible prior to the 
                        event at 503.731.8230 and/or fax 503.731.3266.
                    </p>

                    <p>
                        <b>Request an Interpreter:</b> To request an interpreter for the public meeting, please call 503.731.8230 at least 48 hours before the meeting. 
                        Para solicitar un intérprete para la reunión pública, llame al 503.731.8230 al menos 48 horas antes de la reunión. 
                        Để yêu cầu một thông dịch viên cho buổi họp công khai, xin gọi điện thoại số 503.731.8230 tối thiểu 48 giờ trước buổi họp. 
                        Если Вы нуждаетесь в услугах устного переводчика на общественном собрании, пожалуйста, позвоните по тел. 503.731.8230 как минимум за 48 часов до начала собрания. 
                        您参加公共会议时若需要口译员，请于会前至少 48 小时拨打503.731.3490.           
                    </p>
                    <hr/>
-->
                    <!-- 
                    <h5>
                        <span class="lft "></span>
                        <span class="mid ">Join Us For Upcoming Events</span>
                        <span class="rt "></span>
                    </h5>

                    <h2 style="color:#C8584C; ">Community Advisory Group Meeting</h2>
                    <h3>Wednesday, June 15th
                        <br>from 6:00 to 8:00 pm</h3>
                    <h4>Human Solutions
                        <br>12350 SE Powell Boulevard, Portland, OR 97236</h4>

                    <p class="centeredtext ">
                        <strong>There are no scheduled events at this time. Check back for future events.</strong>
                    </p>

                    <h4 class="centeredtext ">
                        <strong style="font-style:italic; ">There are no scheduled events at this time. Check back for future events.</strong>
                    </h4>

                    <blockquote>
                        <p>A light supper will be provided and available at 5:30pm</p>
                    </blockquote>

                    <h2 style="color:#C8584C; ">Project Open House</h2>

                    <p>If you were unable to attend the most recent public open house on Tuesday, March 21, 2017, you can still
                        view the display boards and project footprint at the links below. These displays provide information
                        about the project schedule, typical cross section, bike facilities, intersections and driveway access.
                        In addition to these items, the project footprint shows things like sidewalks, bus stop locations,
                        lighting, and more. Take a look and let us know if you have any questions.</p>

                    <h3>
                        <a href="doc/OPTSP_Boards_03172017.pdf " class="btn red-btn " target="_blank ">Open House Display Boards &mdash; March 2017</a>

                    </h3>

                    <h3>
                        <a href="doc/OPTSP_ProjectMap_03172017.pdf " class="btn red-btn " target="_blank ">Project Map &mdash; March 2017</a>

                    </h3>

                    <p style="padding-top: 12px; ">Sign up for the project email list or submit a question or comment
                        <a href="http://outerpowellsafety.org/contact.php ">here</a>.</p>

                    <p>
                        <strong>
                            KEEP CHECKING THE PROJECT WEBSITE FOR UPDATES AND FUTURE EVENTS.
                        </strong>
                    </p>

                    <hr />

                    <p>
                        The Oregon Department of Transportation (ODOT) is hosting an informational open house for the US 26: Outer Powell Transportation
                        Safety Project. Join us and learn about upcoming improvements to SE Powell Boulevard from SE 122nd
                        Avenue to SE 136th Avenue.
                    </p>


                    <p>
                        <b>Accessible Event Information:</b> This event/location is accessible to people with disabilities.
                        Sign language, interpreter service, assisted listening devices, materials in alternate format, such
                        as Braille, large print, electronic formats and audio cassette tape, and other accommodations can
                        be available upon advance request. Please contact Susan Hanson as soon as possible prior to the event
                        at 503.731.3490 and/or fax 503.731.3266.
                    </p>


                    <p style="margin-bottom: 0px;font-size: 16px;line-height: 24px; ">
                        <b style="margin-bottom: 0px; ">Request an Interpreter:</b>
                        <div style="font-size: 16px;line-height: 24px; ">To request an interpreter for the public meeting, please call 503.731.3490 at least 48 hours before
                            the meeting.</div>
                        <div style="font-size: 16px;line-height: 24px; ">Para solicitar un intérprete para la reunión pública, llame al 503.731.3490 al menos 48 horas
                            antes de la reunión. </div>
                        <div style="font-size: 16px;line-height: 24px; ">Để yêu cầu một thông dịch viên cho buổi họp công khai, xin gọi điện thoại số 503.731.3490
                            tối thiểu 48 giờ trước buổi họp. </div>
                        <div style="font-size: 16px;line-height: 24px; ">Если Вы нуждаетесь в услугах устного переводчика на общественном собрании, пожалуйста, позвоните
                            по тел. 503.731.3490 как минимум за 48 часов до начала собрания. </div>
                        <div style="font-size: 16px;line-height: 24px; ">
                            您参加公共会议时若需要口译员,请于会前至少 48 小时拨打 503.731.3490.
                        </div>

                        <br />
                        <b style="margin-bottom: 0px;font-size: 16px;line-height: 24px; ">For more information contact us at:</b>
                        <br />
                        <div style="margin-bottom: 0px;font-size: 16px;line-height: 24px; ">
                            Susan Hanson | ODOT Community Affairs Manager
                            <br /> Susan.C.Hanson@odot.state.or.us
                            <br /> 503.731.3490
                            <br />

                        </div>
                    </p>



                    <h2 style="color:#C8584C; ">Decision Committee Meeting</h2>
                    <h3>Tuesday, December 8th
                        <br>from 5:30 to 7:30 pm</h3>
                    <h4>David Douglas High School - Library
                        <br>1001 SE 135th Avenue, Portland, OR 97233</h4>
                    <p class="centeredtext "></p>
                    <br>
                    <blockquote>
                        <p>A light supper will be provided and available at 5:00pm</p>
                    </blockquote>

                    <p>To request an interpreter for the public meeting, please call 503.731.3490 at least 48 hours before the
                        meeting.
                        <br> 您参加公共会议时若需要口译员，请于会前至少 48 小时拨打503.731.3490.
                        <br> Para solicitar un intérprete para la reunión pública, llame al 503.731.3490 al menos 48 horas antes
                        de la reunión.
                        <br> Để yêu cầu một thông dịch viên cho buổi họp công khai, xin gọi điện thoại số 503‐417‐ 5447 tối thiểu
                        48 giờ trước buổi họp.
                        <br> Если Вы нуждаетесь в услугах устного переводчика на общественном собрании, пожалуйста, позвоните
                        по тел. 503.731.3490 как минимум за 48 часов до начала собрания.
                    </p> 
-->


                </div>


            </div>

            <div class="span3 ">
            
                <!-- right sidebar -->
                
                <div class="blogSidebar ">
                    <h4>Submit a comment</h4>
                    <p>Submit a comment anytime online to let us know what you think.</p>
                    <a href="contact.php " class="btn red-btn ">Comment</a>
                    <br>
                    <br>
                    <h4>Other project meetings</h4>

                    <p>
                        <strong>Noise Board Hearing</strong>
                        <br> 2018 June 30th from 6 pm to 8 pm
                    </p>

                    <p>
                        <strong>Community Advisory Group Meeting</strong>
                        <br />2017 March 20th from 6pm to 7pm
                        <br> 2016 June 15th from 6pm to 8pm
                        <br /> 2015 August 27th from 6pm to 8pm
                        <br> 2015 June 8th from 6pm to 8pm
                        <br> 2015 March 18th from 6pm to 8pm
                        <br> 2015 January 26th from 6pm to 8pm
                        <br> 2014 December 3rd from 5:30pm to 7:30pm</p>
                    <p>
                        <strong>Decision Committee Meeting</strong>
                        <br> 2014 December 11th from 5:30pm to 7:30pm
                        <br /> 2015 June 23rd from 5:30pm to 7:30pm
                        <br /> 2015 March 31st from 5:30pm to 7:30pm
                        <br /> 2106 January 26th from 6pm to 8pm</p>
                    <p>
                        <strong>Community Walk</strong>
                        <br> February 7th from 10am to Noon</p>
                    <p>
                        <strong>Project Open House</strong>
                        <br/>2018 September 27th from 5:30pm to 7:30pm
                        <br/>2017 March 21st from 5:30pm to 7:30pm
                        <br> 2015 September 16th from 5:30pm to 7:30pm
                        <br> 2015 March 9th from 5:30pm to 7:30pm
                        <br> 2014 December 9th from 6pm to 8pm</p>
                    <br>
                    <h4>COMMUNITY MEETING MATERIALS</h4>
                    <!-- <ul class="documents ">
						<li><a href="doc/OPTSP_Community_BikeRide_Movies.pdf ">Community Bike Ride Flyer</a></li>
						<li><a href="doc/infographic.pdf ">Infographic</a></li>
						<li><a href="doc/factsheet-english.pdf ">Factsheet (English)</a></li>
						<li><a href="doc/factsheet-russian.pdf ">Factsheet (Russian)</a></li>
						<li><a href="doc/factsheet-vietnamese.pdf ">Factsheet (Vietnamese)</a></li>
						<li><a href="doc/factsheet-spanish.pdf ">Factsheet (Spanish)</a></li>
						<li><a href="doc/factsheet-chinese.pdf ">Factsheet (Chinese)</a></li>
					</ul> -->
                    <p>
                        Download a project fact sheet or get more information about past meetings and public events.
                        <a href="resources.php " class="btn red-btn ">RESOURCES </a>
                    </p>
                </div>
            </div>

        </div>


    </div>


</div>


<?php include 'footer.php'; ?>